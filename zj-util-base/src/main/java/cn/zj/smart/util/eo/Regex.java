package cn.zj.smart.util.eo;

/**
 * @author Mr. xi.yang<br/>
 * @version V1.0 <br/>
 * @description: 分隔符号 <br/>
 * @date 2017-09-29 上午 11:01 <br/>
 */
public enum Regex {
    /**
     * 空格分隔
     */
    BLANK_SPACE(" ", " "),
    /**
     * 逗号分隔
     */
    COMMA(",", "，"),
    /**
     * 冒号分隔
     */
    COLON(":", "："),
    /**
     * 点分隔
     */
    POINT("\\.", "。"),
    /**
     * 分号分隔
     */
    SEMICOLON(";", "；")
    ;

    Regex(String regex, String perRegex) {
        this.regex = regex;
        this.perRegex = perRegex;
    }

    private String regex;
    private String perRegex;

    public String getRegex() {
        return regex;
    }

    public String getPerRegex() {
        return perRegex;
    }
}
