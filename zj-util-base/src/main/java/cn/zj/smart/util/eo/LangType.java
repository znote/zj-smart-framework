package cn.zj.smart.util.eo;


/**
 * 语言
 * @author xi.yang
 * @create 2019-08-27 9:32
 **/
public enum LangType {
    /**
     * 英语
     * English
     */
    ENGLISH(1L,"en_US"),
    /**
     * 葡萄牙语（巴西）
     * Português
     */
    PORTUGUESE(2L, "pt_BR"),
    /**
     * 西班牙语（西班牙）
     * Español
     */
    SPANISH(4L, "es_ES"),
    ;
    private long id;
    private String language;

    LangType(long id, String language) {
        this.id = id;
        this.language = language;
    }

    public static boolean hasLang(long langId, LangType langType) {
        return (langId & langType.getId()) > 0;
    }

    public static long addLang(long langId, LangType langType) {
        return langId | langType.getId();
    }

    public static long removeLang(long langId, LangType langType) {
        return langId & (~langType.getId());
    }

    public long getId() {
        return id;
    }

    public String getLanguage() {
        return language;
    }
}
