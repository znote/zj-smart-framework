package cn.zj.smart.util.bean;

import java.io.Serializable;

/**
 * 筛选选项
 * @author xi.yang
 * @create 2019-05-06 14:58
 **/
public class Criteria implements Serializable {
    /**
     * 页数,从1开始
     */
    private int page = 1;
    /**
     * 每页条数,默认10条
     */
    private int size = 10;

    public void setPage(int page) {
        if (page <= 0) {
            return;
        }
        this.page = page;
    }

    public void setSize(int size) {
        if (size <= 0) {
            return;
        }
        this.size = size;
    }

    public int getPage() {
        return page;
    }

    public int getSize() {
        return size;
    }

}
