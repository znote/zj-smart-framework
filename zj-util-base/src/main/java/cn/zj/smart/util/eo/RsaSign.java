package cn.zj.smart.util.eo;

/**
 * RSA签名算法类型
 * @author xi.yang
 * @create 2020-10-20 17:06
 **/
public enum RsaSign {
    MD2withRSA,
    MD5withRSA,
    SHA1withRSA,
    SHA256withRSA,
    SHA384withRSA,
    SHA512withRSA
}
