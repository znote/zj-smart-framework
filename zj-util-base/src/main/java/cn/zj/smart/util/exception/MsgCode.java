package cn.zj.smart.util.exception;

/**
 * @author xi.yang
 * @create 2020-06-15 11:53
 **/
public interface MsgCode {
    String getMessage();
}
