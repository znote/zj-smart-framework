package cn.zj.smart.util.bean;

import java.io.Serializable;

/**
 * web请求统一返回的对象
 *
 * @author ouyang
 */
public class ApiResult<T> implements Serializable {
    private static final String SUCCESS_STR = "SUCCESS";
    /**
     * 状态码
     */
    public int code;

    /**
     * 返回内容
     */
    public String msg;

    /**
     * 数据对象
     */
    public T data;

    /**
     * 状态类型
     */
    public enum CodeType {
        /**
         * 成功
         */
        SUCCESS(200),
        /**
         * 警告
         */
        WARN(300),
        /**
         * 权限校验错误
         */
        AUTH(401),
        /**
         * 错误
         */
        ERROR(500),
        ;
        private final int value;

        CodeType(int value) {
            this.value = value;
        }

        public int getValue() {
            return value;
        }
    }

    public int getCode() {
        return code;
    }

    public String getMsg() {
        return msg;
    }

    public T getData() {
        return data;
    }

    private ApiResult(CodeType code, String msg, T data) {
        this.code = code.getValue();
        this.msg = msg;
        this.data = data;
    }


    /**
     * 返回成功消息
     *
     * @return 成功消息
     */
    public static ApiResult success() {
        return ApiResult.success(SUCCESS_STR);
    }

    /**
     * 返回成功数据
     *
     * @return 成功消息
     */
    public static <T> ApiResult<T> success(T data) {
        return ApiResult.success(SUCCESS_STR, data);
    }

    /**
     * 返回成功消息
     *
     * @param msg 返回内容
     * @return 成功消息
     */
    public static ApiResult success(String msg) {
        return ApiResult.success(msg, null);
    }

    /**
     * 返回成功消息
     *
     * @param msg  返回内容
     * @param data 数据对象
     * @return 成功消息
     */
    public static <T> ApiResult<T> success(String msg, T data) {
        return new ApiResult(CodeType.SUCCESS, msg, data);
    }

    /**
     * 返回警告消息
     *
     * @param msg 返回内容
     * @return 警告消息
     */
    public static ApiResult warn(String msg) {
        return ApiResult.warn(msg, null);
    }

    /**
     * 返回警告消息
     *
     * @param msg  返回内容
     * @param data 数据对象
     * @return 警告消息
     */
    public static <T> ApiResult<T> warn(String msg, T data) {
        return new ApiResult(CodeType.WARN, msg, data);
    }

    /**
     * 返回错误消息
     *
     * @param msg 返回内容
     * @return 警告消息
     */
    public static ApiResult error(String msg) {
        return ApiResult.error(msg, null);
    }

    public static ApiResult auth(String msg) {
        return new ApiResult(CodeType.AUTH, msg, null);
    }

    /**
     * 返回错误消息
     *
     * @param msg  返回内容
     * @param data 数据对象
     * @return 警告消息
     */
    public static <T> ApiResult<T> error(String msg, T data) {
        return new ApiResult<>(CodeType.ERROR, msg, data);
    }

}
