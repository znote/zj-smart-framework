package cn.zj.smart.util.eo;

import java.time.format.DateTimeFormatter;

/**
 * 时间日期格式化
 * @author xi.yang
 * @create 2020-12-25 18:06
 **/
public enum DateDtf {
    yyyyMMdd_HHmmss(DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss")),
    yyyyMMdd_HHmmss_sp(DateTimeFormatter.ofPattern("yyyy/MM/dd HH:mm:ss")),
    yyyyMMdd_HHmmss_mini(DateTimeFormatter.ofPattern("yyyyMMddHHmmss")),
    yyyyMMdd_HHmm(DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm")),
    yyyyMMdd_HHmm_sp(DateTimeFormatter.ofPattern("yyyy/MM/dd HH:mm")),
    MMdd_HHmm(DateTimeFormatter.ofPattern("MM-dd HH:mm"),true),
    yyyyMMdd(DateTimeFormatter.ofPattern("yyyy-MM-dd"),true),
    yyyyMMdd_sp(DateTimeFormatter.ofPattern("yyyy/MM/dd"),true),
    yyyyMMdd_ch(DateTimeFormatter.ofPattern("yyyy年MM月dd日"),true),
    yyyyMM(DateTimeFormatter.ofPattern("yyyy-MM"),true),
    yyMMdd_mini(DateTimeFormatter.ofPattern("yyMMdd"),true),
    yyyyMMdd_mini(DateTimeFormatter.ofPattern("yyyyMMdd"),true),
    HHmmss(DateTimeFormatter.ofPattern("HH:mm:ss"),true),
    HHmm(DateTimeFormatter.ofPattern("HH:mm"),true),
    ;
    private DateTimeFormatter formatter;
    /**
     * 字符串转换成时间要单独处理
     */
    private boolean strProcess;
    DateDtf(DateTimeFormatter formatter) {
        this.formatter = formatter;
    }

    DateDtf(DateTimeFormatter formatter, boolean strProcess) {
        this.formatter = formatter;
        this.strProcess = strProcess;
    }

    public DateTimeFormatter getFormatter() {
        return formatter;
    }

    public boolean isStrProcess() {
        return strProcess;
    }
}
