package cn.zj.smart.util.eo;

public enum PlatformType {
    ANDROID(1,"android"),
    IOS(2,"ios"),
    WEB(3,"web"),
    OTHER(4,"other");

    private int id;
    private String desc;

    PlatformType(int id, String desc) {
        this.id = id;
        this.desc = desc;
    }

    public int getId() {
        return id;
    }

    public String getDesc() {
        return desc;
    }
}
