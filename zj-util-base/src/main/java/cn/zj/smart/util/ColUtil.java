package cn.zj.smart.util;

import java.util.ArrayList;
import java.util.List;

/**
 * 集合相关处理工具类
 * @author xi.yang
 * @create 2022-01-04 9:50
 **/
public class ColUtil {
    /**
     * 根据下标移出数组元素
     * @param list
     * @param idx
     * @param <T>
     */
    public static <T>void removeList(List<T> list,List<Integer> idx){
        int cnt = 0;
        for (int i = 0; i < list.size(); i++) {
            if (idx.contains(cnt)) {
                list.remove(i);
                i--;
            }
            cnt++;
        }
    }

    /**
     * 递归函数,从list中第low个开始到n个位置，取r个数
     * 计算C(n,r)
     */
    public static void combination(int[] list, int r, int low, int n, List<List<Integer>> result) {
        if (low < r) {
            for (int j = low; j < n; j++) {
                if ((low > 0 && list[j] < list[low - 1]) || low == 0) {
                    int temp = list[low];
                    list[low] = list[j];
                    list[j] = temp;
                    combination(list, r, low + 1, n, result);
                    temp = list[low];
                    list[low] = list[j];
                    list[j] = temp;
                }
            }
        }
        if (low == r) {
            List<Integer> temp = new ArrayList<>();
            for (int i = 0; i < r; i++) {
                temp.add(list[i]);
            }
            result.add(new ArrayList<>(temp));
            temp.clear();
        }
    }
}
