package cn.zj.smart.util.exception;

/**
 * @author xi.yang
 * @create 2020-06-15 11:52
 **/
public class ServiceException extends RuntimeException {
    private String message;

    public ServiceException(String message) {
        this.message = message;
    }

    @Override
    public String getMessage() {
        return this.message;
    }

    @Override
    public Throwable fillInStackTrace() {
        return null;
    }
}
