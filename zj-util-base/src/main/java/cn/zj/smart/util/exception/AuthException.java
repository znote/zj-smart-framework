package cn.zj.smart.util.exception;

/**
 * 权限认证异常
 * @author xi.yang
 * @create 2019-09-18 14:42
 **/
public class AuthException extends RuntimeException {
    private static final long serialVersionUID = 1L;
    private MsgCode msgCode;

    public AuthException(MsgCode msgCode) {
        this.msgCode = msgCode;
    }

    @Override
    public String getMessage() {
        return this.msgCode.getMessage();
    }

    @Override
    public Throwable fillInStackTrace() {
        return null;
    }
}
