package cn.zj.smart.util;

import cn.zj.smart.util.bean.CharTag;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * 判断是否重名
 * @author xi.yang
 * @create 2021-09-13 16:00
 **/
public class WorldFilterUtil {
    private Map<Character, CharTag> allChars = new HashMap<>();

    /**
     * 批量添加
     * @param list
     */
    public void addString(List<String> list) {
        for (String s : list) {
            addString(s);
        }
    }

    /**
     * 单个添加
     * @param str
     */
    public void addString(String str) {
        str = str.replace(" ", "");
        if (StringUtil.isBlank(str)) {
            return;
        }
        char start = str.charAt(0);
        CharTag curTag = allChars.get(start);
        if (curTag == null) {
            curTag = new CharTag(start, str.length() == 1);
            allChars.put(start, curTag);
        }
        if (str.length() == 1) {
            curTag.setEnd(true);
        }
        for (int i = 1; i < str.length(); i++) {
            char c = str.charAt(i);
            if (i == str.length() - 1) {
                curTag = curTag.addNext(c, true);
            } else {
                curTag = curTag.addNext(c, false);
            }
        }
    }

    /**
     * 移出
     * @param str
     */
    public void removeString(String str) {
        str = str.replace(" ", "");
        if (StringUtil.isBlank(str)) {
            return;
        }
        char start = str.charAt(0);
        CharTag cur = this.allChars.get(start);
        if (null == cur) {
            return;
        }
        if (str.length() == 1) {
            cur.setEnd(false);
            return;
        }
        for (int i = 1; i < str.length(); i++) {
            if (cur.getNext() == null) {
                return;
            }
            char c = str.charAt(i);
            cur = cur.getNext().get(c);
        }
        if (cur != null) {
            cur.setEnd(false);
        }
    }

    /**
     * 判断是否存在
     * @param str
     * @return
     */
    public boolean isIn(String str) {
        char start = str.charAt(0);
        CharTag cur = this.allChars.get(start);
        if (null == cur) {
            return false;
        }
        if (str.length() == 1 && cur.isEnd()) {
            return true;
        }
        for (int i = 1; i < str.length(); i++) {
            if (cur.getNext() == null) {
                return false;
            }
            char c = str.charAt(i);
            cur = cur.getNext().get(c);
        }
        return cur != null && cur.isEnd();
    }

    public static void main(String[] args) {
        WorldFilterUtil worldFilterUtil = new WorldFilterUtil();
        worldFilterUtil.addString("aabcd");
        worldFilterUtil.addString("abbcd");
        worldFilterUtil.addString("hahah");
        worldFilterUtil.addString("hahah哈哈哇");
        worldFilterUtil.addString("哈哈哈哇");
        worldFilterUtil.addString("a");
        System.out.println(worldFilterUtil.isIn("aa"));
        System.out.println(worldFilterUtil.isIn("a"));
        System.out.println(worldFilterUtil.isIn("hahah"));
        System.out.println(worldFilterUtil.isIn("哈哈哇"));
        System.out.println("=============");
        System.out.println(worldFilterUtil.isIn("哈哈哈哇"));
        worldFilterUtil.removeString("哈哈哈哇");
        System.out.println(worldFilterUtil.isIn("哈哈哈哇"));
        System.out.println("==========");
        System.out.println(worldFilterUtil.isIn("a"));
        worldFilterUtil.removeString("a");
        System.out.println(worldFilterUtil.isIn("a"));
    }
}
