package cn.zj.smart.util.exception;

/**
 * @author xi.yang
 * @create 2020-06-15 11:52
 **/
public class WarnException extends RuntimeException {
    private MsgCode msgCode;

    public WarnException(MsgCode msgCode) {
        this.msgCode = msgCode;
    }

    @Override
    public String getMessage() {
        return this.msgCode.getMessage();
    }

    @Override
    public Throwable fillInStackTrace() {
        return null;
    }

}
