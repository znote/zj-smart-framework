package cn.zj.smart.util.bean;

import java.io.Serializable;
import java.util.List;

/**
 * 分页对象
 * @param
 */
public class PageData<T> implements Serializable {
    /**
     * 总条数
     */
    private long totalNum;
    /**
     * 总页数
     */
    private long totalPage;
    /**
     * 当前页数
     */
    private int page;
    /**
     * 每页条数
     */
    private int size;
    /**
     * 数据
     */
    private List<T> list;

    private PageData() {
    }

    public static <T> PageData<T> of(List<T> data, long totalNum, Criteria criteria) {
        PageData<T> pageData = new PageData<>();
        pageData.list = data;
        pageData.page = criteria.getPage();
        pageData.size = criteria.getSize();
        pageData.totalNum = totalNum;
        pageData.totalPage = pageData.totalNum / pageData.size+ (pageData.totalNum % pageData.size == 0 ? 0 : 1);
        return pageData;
    }

    public long getTotalNum() {
        return totalNum;
    }

    public long getTotalPage() {
        return totalPage;
    }

    public int getPage() {
        return page;
    }

    public int getSize() {
        return size;
    }

    public List<T> getList() {
        return list;
    }
}
