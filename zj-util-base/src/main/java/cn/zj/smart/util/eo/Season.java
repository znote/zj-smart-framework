package cn.zj.smart.util.eo;

/**
 * @author Mr. xi.yang<br/>
 * @version V1.0 <br/>
 * @description: 季节(季度) <br/>
 * @date 2017-09-26 下午 3:10 <br/>
 */
public enum Season {
	/**
	 * 春季/一季度
	 */
	SPRING(1),
	/**
	 * 夏季/二季度
	 */
	SUMMER(2),
	/**
	 * 秋季/三季度
	 */
	AUTUMN(3),
	/**
	 * 冬季/四季度
	 */
	WINTER(4),
	;

	private int id;

	Season(int id) {
		this.id = id;
	}

	public int getId() {
		return id;
	}
}
