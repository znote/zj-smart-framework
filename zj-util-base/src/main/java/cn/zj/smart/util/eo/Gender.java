package cn.zj.smart.util.eo;

/**
 * 性别
 */
public enum Gender {
    /**
     * 男
     */
    MALE(1),
    /**
     * 女
     */
    FEMALE(0),
    /**
     * 保密
     */
    SECURITY(2),
    ;

    private int id;

    Gender(int id) {
        this.id = id;
    }

    public int getId() {
        return id;
    }

    public static Gender findById(int id) {
        for (Gender value : Gender.values()) {
            if (value.getId() == id) {
                return value;
            }
        }
        return null;
    }
}
