package cn.zj.smart.util;

import cn.zj.smart.util.eo.Regex;

import java.util.UUID;

/**
 * 字符串处理相关类
 *
 * @author xi.yang
 * @create 2020-10-23 10:53
 **/
public class StringUtil {
    /**
     * 分隔符分割字符串
     *
     * @param content
     * @param regex
     * @return
     */
    public static String[] splitByRegex(String content, Regex regex) {
        String[] result = {};
        if (content == null) {
            return result;
        }
        String string = content.trim().replace(regex.getPerRegex(), regex.getRegex());
        return string.split(regex.getRegex());
    }

    /**
     * 参数拼成字符串
     *
     * @param args
     * @return
     */
    public static String appendStr(Object... args) {
        StringBuffer sb = new StringBuffer();
        for (Object arg : args) {
            sb.append(arg);
        }
        return sb.toString();
    }

    /**
     * 判断字符串是否为空
     *
     * @param cs
     * @return
     */
    public static boolean isBlank(final CharSequence cs) {
        int strLen;
        if (cs == null || (strLen = cs.length()) == 0) {
            return true;
        }
        for (int i = 0; i < strLen; i++) {
            if (!Character.isWhitespace(cs.charAt(i))) {
                return false;
            }
        }
        return true;
    }

    public static boolean isNotBlank(final CharSequence cs) {
        return !isBlank(cs);
    }

    public static boolean isAnyBlank(CharSequence... cses) {
        for (CharSequence cs : cses) {
            if (isBlank(cs)) {
                return true;
            }
        }
        return false;
    }

    /**
     * 获取uuid，不含"-"
     *
     * @return
     */
    public static String getUUID() {
        return UUID.randomUUID().toString().replace("-", "");
    }

    /**
     * 隐藏手机号
     *
     * @param phone
     * @return
     */
    public static String hiddenPhone(String phone) {
        return phone.replaceAll("(\\d{3})\\d{4}(\\d{4})", "$1****$2");
    }

    /**
     * 截取字符串
     *
     * @param line
     * @param start
     * @param end
     * @return
     */
    public static String subString(String line, String start, String end) {
        return line.substring(line.indexOf(start), line.indexOf(end)).replace(start, "");
    }

    /**
     * 首字母小写
     *
     * @return
     */
    public static String lowerCaseFirstChar(String s) {
        if (Character.isLowerCase(s.charAt(0))) {
            return s;
        }
        return Character.toLowerCase(s.charAt(0)) +
                s.substring(1);
    }

    /**
     * 首字母大写
     *
     * @return
     */
    public static String upperCaseFirstChar(String s) {
        if (Character.isUpperCase(s.charAt(0))) {
            return s;
        }
        return Character.toUpperCase(s.charAt(0)) +
                s.substring(1);
    }
}