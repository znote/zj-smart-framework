package cn.zj.smart.util;

import java.util.HashMap;
import java.util.Map;

/**
 * @author xi.yang
 * @create 2020-06-28 15:14
 **/
public class MapUtil {
    public static <K,V> Map<K, V> of(K k, V v) {
        Map<K, V> result = new HashMap<>();
        result.put(k, v);
        return result;
    }

    public static <K,V> Map<K, V> of(K k1, V v1,K k2,V v2) {
        Map<K, V> result = new HashMap<>();
        result.put(k1, v1);
        result.put(k2, v2);
        return result;
    }

    public static <K,V> Map<K, V> of(K k1, V v1,K k2,V v2,K k3,V v3) {
        Map<K, V> result = new HashMap<>();
        result.put(k1, v1);
        result.put(k2, v2);
        result.put(k3, v3);
        return result;
    }
}
