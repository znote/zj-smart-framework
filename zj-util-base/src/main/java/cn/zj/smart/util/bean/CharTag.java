package cn.zj.smart.util.bean;

import java.util.HashMap;
import java.util.Map;

/**
 * @author xi.yang
 * @create 2021-09-13 15:49
 **/
public class CharTag {
    private char cur;
    private boolean isEnd;
    private Map<Character, CharTag> next;

    public CharTag(char cur, boolean isEnd) {
        this.cur = cur;
        this.isEnd = isEnd;
    }

    public char getCur() {
        return cur;
    }

    public void setCur(char cur) {
        this.cur = cur;
    }

    public boolean isEnd() {
        return isEnd;
    }

    public void setEnd(boolean end) {
        isEnd = end;
    }

    public Map<Character, CharTag> getNext() {
        return next;
    }

    public void setNext(Map<Character, CharTag> next) {
        this.next = next;
    }

    public CharTag addNext(char c, boolean end) {
        CharTag charTag;
        if (this.next == null) {
            Map<Character, CharTag> curNext = new HashMap<>();
            charTag = new CharTag(c, end);
            curNext.put(c, charTag);
            this.next = curNext;
            return charTag;
        }
        charTag = this.next.get(c);
        if (null == charTag) {
            charTag = new CharTag(c, end);
            this.next.put(c, charTag);
        } else if (!charTag.isEnd) {
            if (end) {
                charTag.setEnd(true);
            }
        }
        return charTag;
    }
}