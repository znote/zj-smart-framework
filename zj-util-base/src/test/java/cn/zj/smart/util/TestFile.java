package cn.zj.smart.util;

import java.nio.file.Paths;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

/**
 * @author xi.yang
 * @create 2021-06-15 16:40
 **/
public class TestFile {
    public static void main(String[] args) {
        List<String> lines = FileUtil.readLine(Paths.get("C:\\Users\\d.cn\\Desktop\\icons.dart"));
        Set<String> allIcon = new HashSet<>();
        for (String line : lines) {
            if (line.contains("static const IconData ")) {
                String tag = StringUtil.subString(line, "static const IconData ", " = IconData");
                if (tag.endsWith("_sharp") || tag.endsWith("_rounded") || tag.endsWith("_outlined")) {
                    continue;
                }
                System.out.println("\""+tag+"\": Icons."+tag+",");
            }
        }
    }

}
