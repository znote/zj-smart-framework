package cn.zj.smart.util;

import cn.zj.smart.log4j2.Log;
import org.junit.Test;

import java.time.Clock;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.ZoneId;
import java.time.temporal.TemporalField;

/**
 * @author xi.yang
 * @create 2021-05-24 9:57
 **/
public class DateUtilTest {
    public static void main(String[] args) {
        DateUtil.initZone(ZoneId.of("UTC"));
        LocalDate localDate = LocalDate.of(2021, 5, 24);
        Log.log.info(DateUtil.localDate2Long(localDate));
        LocalDateTime localDateTime = LocalDateTime.of(2021, 5, 23, 22, 0);
        Log.log.info(DateUtil.localDateTime2Long(localDateTime));
    }

    @Test
    public void name() {
        long curTime = Clock.systemUTC().instant().getEpochSecond();
        LocalDateTime localDateTime = DateUtil.long2LocalDateTime(curTime);
        System.out.println(localDateTime);
    }

    @Test
    public void isSameWeekTest() {
        assert DateUtil.isSameWeek(LocalDate.of(2021,10,25), LocalDate.of(2021, 10, 31));
        assert !DateUtil.isSameWeek(LocalDate.of(2021,10,24), LocalDate.of(2021, 10, 31));
        assert DateUtil.isSameWeek(LocalDate.of(2021,12,28), LocalDate.of(2022, 1, 2));
    }
}