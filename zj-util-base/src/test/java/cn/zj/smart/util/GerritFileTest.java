package cn.zj.smart.util;

import java.io.File;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.List;

/**
 * gerrit修改地址后，需要改动已经clone的项目配置
 * 这个脚本完成ip替换
 * @author xi.yang
 * @create 2021-06-15 16:40
 **/
public class GerritFileTest {
    public static void main(String[] args) {
        processOne("D:\\C101\\server");
    }

    private static void processOne(String path) {
        String[] allRoot = FileUtil.getFileNames(path);
        for (String fileName : allRoot) {
            String filePath = path + File.separator + fileName;
            File file = FileUtil.getFile(filePath);
            if (file.isFile()) {
                continue;
            }
            if (".git".equals(fileName)) {
                replaceFileOne(filePath + File.separator + "config");
                replaceFileOne(filePath + File.separator + "FETCH_HEAD");
            } else {
                processOne(filePath);
            }
        }
    }

    private static void replaceFileOne(String s) {
        File file = new File(s);
        if (!file.exists()) {
            return;
        }
        List<String> lines = FileUtil.readLine(Paths.get(s));
        boolean write = false;
        List<String> nLines = new ArrayList<>();
        for (String line : lines) {
            if (line.contains("115.182.62.11")) {
                write = true;
                nLines.add(line.replace("115.182.62.11", "115.182.198.182"));
            } else {
                nLines.add(line);
            }
        }
        if (write) {
            FileUtil.writeFile(s, String.join("\r\n", nLines));
        }
        System.out.println(s + "=============" + write);
    }
}
