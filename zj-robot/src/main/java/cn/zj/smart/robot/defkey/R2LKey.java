package cn.zj.smart.robot.defkey;

import cn.zj.smart.robot.AdRobot;
import cn.zj.smart.robot.Keys;

import java.awt.*;
import java.awt.event.KeyEvent;

public class R2LKey implements Keys,DefineKey {
    @Override
    public int[] keys() {
        return null;
    }

    @Override
    public void doRobot(boolean reverse) {
        try {
            Robot robot = new AdRobot(reverse);
            robot.setAutoWaitForIdle(true);
            robot.setAutoDelay(30);
            robot.keyPress(KeyEvent.VK_D);
            robot.keyPress(KeyEvent.VK_S);
            robot.keyRelease(KeyEvent.VK_D);
            robot.keyPress(KeyEvent.VK_A);
            robot.keyRelease(KeyEvent.VK_S);
            robot.keyRelease(KeyEvent.VK_A);
        } catch (Exception e) {
        }
    }
}
