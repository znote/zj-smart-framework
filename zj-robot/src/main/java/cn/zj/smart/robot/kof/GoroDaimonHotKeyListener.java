package cn.zj.smart.robot.kof;

import cn.zj.smart.robot.*;
import cn.zj.smart.robot.defkey.*;
import com.melloware.jintellitype.HotkeyListener;

import java.awt.event.KeyEvent;

/**
 * 大门
 */
public class GoroDaimonHotKeyListener implements HotkeyListener {
    private KeyRobot keyRobot;

    public GoroDaimonHotKeyListener(KeyRobot keyRobot) {
        this.keyRobot = keyRobot;
    }

    @Override
    public void onHotKey(int i) {
        switch (i) {
            case KeyEvent.VK_0:
                keyRobot.press(
                        new L2RKey(), new L2RKey(), new OrderKey(KeyEvent.VK_I),
                        new TimeKey(1500),
                        new R2LKey(), new OrderKey(KeyEvent.VK_I),
                        new TimeKey(1800),
                        new R2RWithIKey(),
                        new TimeKey(3400),
                        new L2LWithJKey()
                );
                break;
            case KeyEvent.VK_9:
                keyRobot.press(
                        new R2RWithJKey()
                );
                break;
            case KeyEvent.VK_8:
                keyRobot.press(
                        new R2LKey(), new R2LKey(), new OrderKey(KeyEvent.VK_J)
                );
                break;
        }
    }
}
