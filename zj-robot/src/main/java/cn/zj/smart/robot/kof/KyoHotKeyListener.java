package cn.zj.smart.robot.kof;

import cn.zj.smart.robot.*;
import cn.zj.smart.robot.defkey.L2RKey;
import cn.zj.smart.robot.defkey.M2L2RKey;
import cn.zj.smart.robot.defkey.M2RKey;
import cn.zj.smart.robot.defkey.R2LKey;
import com.melloware.jintellitype.HotkeyListener;

import java.awt.event.KeyEvent;

/**
 * 草稚京
 */
public class KyoHotKeyListener implements HotkeyListener {
    private KeyRobot keyRobot;

    public KyoHotKeyListener(KeyRobot keyRobot) {
        this.keyRobot = keyRobot;
    }

    @Override
    public void onHotKey(int i) {
        switch (i) {
            case KeyEvent.VK_0:
                // 终极奥义 无式+挑拨
                keyRobot.press(new M2RKey(),new M2RKey(), new OrderKey(KeyEvent.VK_J));
                break;
            case KeyEvent.VK_9:
                keyRobot.press(new M2L2RKey(), new OrderKey(KeyEvent.VK_J));
                break;
            case KeyEvent.VK_1:
                keyRobot.press(new M2RKey(), new OrderKey(KeyEvent.VK_J), new M2RKey(), new OrderKey(KeyEvent.VK_J), new OrderKey(KeyEvent.VK_U), new OrderKey(KeyEvent.VK_I));
                break;
                case KeyEvent.VK_2:
                keyRobot.press(new M2RKey(), new OrderKey(KeyEvent.VK_J), new TimeKey(200), new R2LKey(), new OrderKey(KeyEvent.VK_J), new TimeKey(200), new OrderKey(KeyEvent.VK_J));
                break;
            case KeyEvent.VK_8:
                keyRobot.press(new M2RKey(), new OrderKey(KeyEvent.VK_J), new TimeKey(200), new R2LKey(), new OrderKey(KeyEvent.VK_J), new TimeKey(200), new OrderKey(KeyEvent.VK_K));
                break;
            case KeyEvent.VK_7:
                keyRobot.press(new M2RKey(), new OrderKey(KeyEvent.VK_U), new L2RKey(), new OrderKey(KeyEvent.VK_J), new MultiKey(KeyEvent.VK_D, KeyEvent.VK_J));
                break;
        }
    }
}
