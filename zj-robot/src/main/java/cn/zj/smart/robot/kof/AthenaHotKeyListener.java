package cn.zj.smart.robot.kof;

import cn.zj.smart.robot.*;
import cn.zj.smart.robot.defkey.M2L2RKey;
import cn.zj.smart.robot.defkey.M2RKey;
import cn.zj.smart.robot.defkey.R2LKey;
import com.melloware.jintellitype.HotkeyListener;

import java.awt.event.KeyEvent;

/**
 * 雅典娜
 */
public class AthenaHotKeyListener implements HotkeyListener {
    private KeyRobot keyRobot;

    public AthenaHotKeyListener(KeyRobot keyRobot) {
        this.keyRobot = keyRobot;
    }

    @Override
    public void onHotKey(int i) {
        switch (i) {
            case KeyEvent.VK_0:
                // 终极奥义 闪光水晶波
                keyRobot.press(new OrderKey(KeyEvent.VK_A), new R2LKey(), new OrderKey(KeyEvent.VK_J));
                break;
            case KeyEvent.VK_9:
                keyRobot.press(new OrderKey(KeyEvent.VK_D, KeyEvent.VK_S, KeyEvent.VK_D, KeyEvent.VK_J));
                break;
            case KeyEvent.VK_8:
                keyRobot.press(new M2RKey(), new M2L2RKey(), new OrderKey(KeyEvent.VK_J));
                break;
        }
    }
}
