package cn.zj.smart.robot.kof;

import cn.zj.smart.robot.KeyRobot;
import cn.zj.smart.robot.MultiKey;
import com.melloware.jintellitype.JIntellitype;
import org.springframework.ui.context.Theme;

import java.awt.event.KeyEvent;

public class KofRobot {
    private static KeyRobot keyRobot = new KeyRobot();

    public static void main(String[] args) {
        // 字母O曝气
        JIntellitype.getInstance().registerHotKey(KeyEvent.VK_O, 0, KeyEvent.VK_O);
        JIntellitype.getInstance().registerHotKey(KeyEvent.VK_Q, 0, KeyEvent.VK_Q);
        int[] sysKeys = {KeyEvent.VK_E, KeyEvent.VK_O, KeyEvent.VK_Q,KeyEvent.VK_N,KeyEvent.VK_M,KeyEvent.VK_P, KeyEvent.VK_F9, KeyEvent.VK_F10, KeyEvent.VK_F11, KeyEvent.VK_F12};
        for (int numKey : sysKeys) {
            JIntellitype.getInstance().registerHotKey(numKey, 0, numKey);
        }
        JIntellitype.getInstance().addHotKeyListener(i -> {
            switch (i) {
                case KeyEvent.VK_Q:
                    keyRobot.reverse();
                    break;
                case KeyEvent.VK_O:
                    // 曝气
                    keyRobot.press(new MultiKey(KeyEvent.VK_J, KeyEvent.VK_K, KeyEvent.VK_U));
                    break;
                case KeyEvent.VK_E:
                    keyRobot.releaseAll();
                    break;
//                case KeyEvent.VK_F9:
//                    JIntellitype.getInstance().addHotKeyListener(new ChangKoehanKeyListener(keyRobot));
//                    break;
//                case KeyEvent.VK_F10:
//                    JIntellitype.getInstance().addHotKeyListener(new BenimaruHotKeyListener(keyRobot));
//                    break;
//                case KeyEvent.VK_F11:
//                    JIntellitype.getInstance().addHotKeyListener(new GoroDaimonHotKeyListener(keyRobot));
//                    break;
            }
        });
        int[] numKeys = {KeyEvent.VK_0, KeyEvent.VK_1, KeyEvent.VK_2, KeyEvent.VK_3, KeyEvent.VK_4, KeyEvent.VK_5, KeyEvent.VK_6, KeyEvent.VK_7, KeyEvent.VK_8, KeyEvent.VK_9};
        for (int numKey : numKeys) {
            JIntellitype.getInstance().registerHotKey(numKey, 0, numKey);
        }
        JIntellitype.getInstance().addHotKeyListener(new CommonKeyListener(keyRobot));
        System.out.println("start success ！！！");
        try {
            Thread.sleep(100000000);
        } catch (InterruptedException e) {
            throw new RuntimeException(e);
        }
    }
}
