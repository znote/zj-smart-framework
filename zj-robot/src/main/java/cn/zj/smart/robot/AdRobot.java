package cn.zj.smart.robot;

import java.awt.*;
import java.awt.event.KeyEvent;

/**
 * 左右反转的按键
 */
public class AdRobot extends Robot {
    private boolean reverse = false;

    public AdRobot(boolean reverse) throws AWTException {
        super();
        this.reverse = reverse;
    }

    public AdRobot() throws AWTException {
        super();
    }

    public AdRobot(GraphicsDevice screen) throws AWTException {
        super(screen);
    }

    @Override
    public synchronized void keyPress(int keycode) {
//        System.out.println(this.reverse+"============"+keycode);
        if (this.reverse && (keycode == KeyEvent.VK_A || keycode == KeyEvent.VK_D)) {
            if (keycode == KeyEvent.VK_A) {
                super.keyPress(KeyEvent.VK_D);
            }else {
                super.keyPress(KeyEvent.VK_A);
            }
        }else {
            super.keyPress(keycode);
        }
    }

    @Override
    public synchronized void keyRelease(int keycode) {
        if (this.reverse && (keycode == KeyEvent.VK_A || keycode == KeyEvent.VK_D)) {
            if (keycode == KeyEvent.VK_A) {
                super.keyRelease(KeyEvent.VK_D);
            }else {
                super.keyRelease(KeyEvent.VK_A);
            }
        }else {
            super.keyRelease(keycode);
        }
    }
}
