package cn.zj.smart.robot;

import cn.zj.smart.robot.defkey.DefineKey;

import java.util.concurrent.CountDownLatch;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.TimeUnit;

public class KeyRobot {
    /**
     * 左右颠倒
     */
    private boolean reverse = false;
    private static ExecutorService singleThreadExecutor = Executors.newSingleThreadExecutor();
    private static ExecutorService fixedThreadPool = Executors.newFixedThreadPool(10);
    private AdRobot robot;

    public void press(Keys... keys) {
        try {
            singleThreadExecutor.shutdownNow();
            singleThreadExecutor = Executors.newSingleThreadExecutor();
            for (Keys key : keys) {
                singleThreadExecutor.submit(() -> {
                    if (key instanceof OrderKey) {
                        try {
                            robot = new AdRobot(this.reverse);
                            robot.setAutoWaitForIdle(true);
                            robot.setAutoDelay(20);
                            for (int i : key.keys()) {
                                robot.keyPress(i);
                                robot.keyRelease(i);
                            }
                        } catch (Exception e) {
                        }
                    } else if (key instanceof MultiKey) {
                        fixedThreadPool.shutdownNow();
                        fixedThreadPool = Executors.newFixedThreadPool(key.keys().length);
                        CountDownLatch countDownLatch = new CountDownLatch(key.keys().length);
                        for (int i : key.keys()) {
                            fixedThreadPool.submit(() -> {
                                try {
                                    robot = new AdRobot(this.reverse);
                                    robot.setAutoWaitForIdle(true);
                                    robot.setAutoDelay(10);
                                    robot.keyPress(i);
                                    robot.keyRelease(i);
                                    countDownLatch.countDown();
                                } catch (Exception e) {
                                    e.printStackTrace();
                                }
                            });
                        }
                        try {
                            countDownLatch.wait();
                        } catch (Exception e) {
                        }
                    } else if (key instanceof DefineKey) {
                        ((DefineKey) key).doRobot(this.reverse);
                    } else if (key instanceof TimeKey) {
                        try {
                            TimeUnit.MILLISECONDS.sleep(((TimeKey) key).getTime());
                        } catch (Exception e) {
                        }
                    }
                });
            }
        } catch (Exception e) {
        }
    }

    public void reverse() {
        this.reverse = !this.reverse;
        if (this.reverse) {
            System.out.println("=====人在右边======");
        } else {
            System.out.println("=====人在左边======");
        }
    }

    public void releaseAll() {
        singleThreadExecutor.shutdownNow();
        fixedThreadPool.shutdownNow();
    }
}
