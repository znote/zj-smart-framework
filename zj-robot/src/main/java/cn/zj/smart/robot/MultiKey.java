package cn.zj.smart.robot;

public class MultiKey implements Keys{
    private int[] keys;

    public MultiKey(int... keys) {
        this.keys = keys;
    }

    @Override
    public int[] keys() {
        return this.keys;
    }
}
