package cn.zj.smart.robot;

public class OrderKey implements Keys {
    private int[] keys;
    public OrderKey(int... keys) {
        this.keys = keys;
    }

    @Override
    public int[] keys() {
        return this.keys;
    }
}
