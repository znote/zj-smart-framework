package cn.zj.smart.robot.kof;

import cn.zj.smart.robot.KeyRobot;
import cn.zj.smart.robot.OrderKey;
import cn.zj.smart.robot.defkey.M2L2RKey;
import cn.zj.smart.robot.defkey.M2RKey;
import cn.zj.smart.robot.defkey.R2LKey;
import com.melloware.jintellitype.HotkeyListener;

import java.awt.event.KeyEvent;

/**
 * 陈国汉
 */
public class ChangKoehanKeyListener implements HotkeyListener {
    private KeyRobot keyRobot;

    public ChangKoehanKeyListener(KeyRobot keyRobot) {
        this.keyRobot = keyRobot;
    }

    @Override
    public void onHotKey(int i) {
        switch (i) {
            case KeyEvent.VK_0:
                // 铁球大暴走
                keyRobot.press(new M2RKey(),new R2LKey(),  new OrderKey(KeyEvent.VK_J));
                break;
        }
    }
}
