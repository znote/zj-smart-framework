package cn.zj.smart.robot.kof;

import cn.zj.smart.robot.KeyRobot;
import cn.zj.smart.robot.MultiKey;
import cn.zj.smart.robot.OrderKey;
import cn.zj.smart.robot.TimeKey;
import cn.zj.smart.robot.defkey.M2RKey;
import cn.zj.smart.robot.defkey.R2LKey;
import com.melloware.jintellitype.HotkeyListener;

import java.awt.event.KeyEvent;

/**
 * 通用的
 */
public class CommonKeyListener implements HotkeyListener {
    private KeyRobot keyRobot;

    public CommonKeyListener(KeyRobot keyRobot) {
        this.keyRobot = keyRobot;
    }

    @Override
    public void onHotKey(int i) {
        switch (i) {
            case KeyEvent.VK_E:
                // 铁球大暴走
                keyRobot.press(
                        new MultiKey(KeyEvent.VK_J,KeyEvent.VK_K,KeyEvent.VK_U,KeyEvent.VK_I),new TimeKey(30),
                        new MultiKey(KeyEvent.VK_J,KeyEvent.VK_K,KeyEvent.VK_U,KeyEvent.VK_I),new TimeKey(30),
                        new MultiKey(KeyEvent.VK_J,KeyEvent.VK_K,KeyEvent.VK_U,KeyEvent.VK_I),new TimeKey(30),
                        new MultiKey(KeyEvent.VK_J,KeyEvent.VK_K,KeyEvent.VK_U,KeyEvent.VK_I),new TimeKey(30),
                        new MultiKey(KeyEvent.VK_J,KeyEvent.VK_K,KeyEvent.VK_U,KeyEvent.VK_I),new TimeKey(30),
                        new MultiKey(KeyEvent.VK_J,KeyEvent.VK_K,KeyEvent.VK_U,KeyEvent.VK_I),new TimeKey(30),
                        new MultiKey(KeyEvent.VK_J,KeyEvent.VK_K,KeyEvent.VK_U,KeyEvent.VK_I),new TimeKey(30),
                        new MultiKey(KeyEvent.VK_J,KeyEvent.VK_K,KeyEvent.VK_U,KeyEvent.VK_I),new TimeKey(30),
                        new MultiKey(KeyEvent.VK_J,KeyEvent.VK_K,KeyEvent.VK_U,KeyEvent.VK_I),new TimeKey(30),
                        new MultiKey(KeyEvent.VK_J,KeyEvent.VK_K,KeyEvent.VK_U,KeyEvent.VK_I),new TimeKey(30),
                        new MultiKey(KeyEvent.VK_J,KeyEvent.VK_K,KeyEvent.VK_U,KeyEvent.VK_I),new TimeKey(30),
                        new MultiKey(KeyEvent.VK_J,KeyEvent.VK_K,KeyEvent.VK_U,KeyEvent.VK_I),new TimeKey(30),
                        new MultiKey(KeyEvent.VK_J,KeyEvent.VK_K,KeyEvent.VK_U,KeyEvent.VK_I),new TimeKey(30),
                        new MultiKey(KeyEvent.VK_J,KeyEvent.VK_K,KeyEvent.VK_U,KeyEvent.VK_I),new TimeKey(30),
                        new MultiKey(KeyEvent.VK_J,KeyEvent.VK_K,KeyEvent.VK_U,KeyEvent.VK_I),new TimeKey(30),
                        new MultiKey(KeyEvent.VK_J,KeyEvent.VK_K,KeyEvent.VK_U,KeyEvent.VK_I),new TimeKey(30),
                        new MultiKey(KeyEvent.VK_J,KeyEvent.VK_K,KeyEvent.VK_U,KeyEvent.VK_I),new TimeKey(30),
                        new MultiKey(KeyEvent.VK_J,KeyEvent.VK_K,KeyEvent.VK_U,KeyEvent.VK_I),new TimeKey(30),
                        new MultiKey(KeyEvent.VK_J,KeyEvent.VK_K,KeyEvent.VK_U,KeyEvent.VK_I),new TimeKey(30),
                        new MultiKey(KeyEvent.VK_J,KeyEvent.VK_K,KeyEvent.VK_U,KeyEvent.VK_I),new TimeKey(30),
                        new MultiKey(KeyEvent.VK_J,KeyEvent.VK_K,KeyEvent.VK_U,KeyEvent.VK_I),new TimeKey(30),
                        new MultiKey(KeyEvent.VK_J,KeyEvent.VK_K,KeyEvent.VK_U,KeyEvent.VK_I),new TimeKey(30),
                        new MultiKey(KeyEvent.VK_J,KeyEvent.VK_K,KeyEvent.VK_U,KeyEvent.VK_I),new TimeKey(30),
                        new MultiKey(KeyEvent.VK_J,KeyEvent.VK_K,KeyEvent.VK_U,KeyEvent.VK_I),new TimeKey(30),
                        new MultiKey(KeyEvent.VK_J,KeyEvent.VK_K,KeyEvent.VK_U,KeyEvent.VK_I),new TimeKey(30),
                        new MultiKey(KeyEvent.VK_J,KeyEvent.VK_K,KeyEvent.VK_U,KeyEvent.VK_I),new TimeKey(30),
                        new MultiKey(KeyEvent.VK_J,KeyEvent.VK_K,KeyEvent.VK_U,KeyEvent.VK_I),new TimeKey(30),
                        new MultiKey(KeyEvent.VK_J,KeyEvent.VK_K,KeyEvent.VK_U,KeyEvent.VK_I),new TimeKey(30),
                        new MultiKey(KeyEvent.VK_J,KeyEvent.VK_K,KeyEvent.VK_U,KeyEvent.VK_I),new TimeKey(30),
                        new MultiKey(KeyEvent.VK_J,KeyEvent.VK_K,KeyEvent.VK_U,KeyEvent.VK_I),new TimeKey(30),
                        new MultiKey(KeyEvent.VK_J,KeyEvent.VK_K,KeyEvent.VK_U,KeyEvent.VK_I),new TimeKey(30),
                        new MultiKey(KeyEvent.VK_J,KeyEvent.VK_K,KeyEvent.VK_U,KeyEvent.VK_I),new TimeKey(30),
                        new MultiKey(KeyEvent.VK_J,KeyEvent.VK_K,KeyEvent.VK_U,KeyEvent.VK_I),new TimeKey(30),
                        new MultiKey(KeyEvent.VK_J,KeyEvent.VK_K,KeyEvent.VK_U,KeyEvent.VK_I),new TimeKey(30),
                        new MultiKey(KeyEvent.VK_J,KeyEvent.VK_K,KeyEvent.VK_U,KeyEvent.VK_I),new TimeKey(30),
                        new MultiKey(KeyEvent.VK_J,KeyEvent.VK_K,KeyEvent.VK_U,KeyEvent.VK_I),new TimeKey(30),
                        new MultiKey(KeyEvent.VK_J,KeyEvent.VK_K,KeyEvent.VK_U,KeyEvent.VK_I),new TimeKey(30),
                        new MultiKey(KeyEvent.VK_J,KeyEvent.VK_K,KeyEvent.VK_U,KeyEvent.VK_I),new TimeKey(30),
                        new MultiKey(KeyEvent.VK_J,KeyEvent.VK_K,KeyEvent.VK_U,KeyEvent.VK_I),new TimeKey(30),
                        new MultiKey(KeyEvent.VK_J,KeyEvent.VK_K,KeyEvent.VK_U,KeyEvent.VK_I),new TimeKey(30),
                        new MultiKey(KeyEvent.VK_J,KeyEvent.VK_K,KeyEvent.VK_U,KeyEvent.VK_I),new TimeKey(30),
                        new MultiKey(KeyEvent.VK_J,KeyEvent.VK_K,KeyEvent.VK_U,KeyEvent.VK_I),new TimeKey(30),
                        new MultiKey(KeyEvent.VK_J,KeyEvent.VK_K,KeyEvent.VK_U,KeyEvent.VK_I),new TimeKey(30),
                        new MultiKey(KeyEvent.VK_J,KeyEvent.VK_K,KeyEvent.VK_U,KeyEvent.VK_I),new TimeKey(30),
                        new MultiKey(KeyEvent.VK_J,KeyEvent.VK_K,KeyEvent.VK_U,KeyEvent.VK_I),new TimeKey(30),
                        new MultiKey(KeyEvent.VK_J,KeyEvent.VK_K,KeyEvent.VK_U,KeyEvent.VK_I),new TimeKey(30),
                        new MultiKey(KeyEvent.VK_J,KeyEvent.VK_K,KeyEvent.VK_U,KeyEvent.VK_I),new TimeKey(30),
                        new MultiKey(KeyEvent.VK_J,KeyEvent.VK_K,KeyEvent.VK_U,KeyEvent.VK_I),new TimeKey(30),
                        new MultiKey(KeyEvent.VK_J,KeyEvent.VK_K,KeyEvent.VK_U,KeyEvent.VK_I),new TimeKey(30),
                        new MultiKey(KeyEvent.VK_J,KeyEvent.VK_K,KeyEvent.VK_U,KeyEvent.VK_I),new TimeKey(30),
                        new MultiKey(KeyEvent.VK_J,KeyEvent.VK_K,KeyEvent.VK_U,KeyEvent.VK_I),new TimeKey(30),
                        new MultiKey(KeyEvent.VK_J,KeyEvent.VK_K,KeyEvent.VK_U,KeyEvent.VK_I),new TimeKey(30),
                        new MultiKey(KeyEvent.VK_J,KeyEvent.VK_K,KeyEvent.VK_U,KeyEvent.VK_I),new TimeKey(30),
                        new MultiKey(KeyEvent.VK_J,KeyEvent.VK_K,KeyEvent.VK_U,KeyEvent.VK_I),new TimeKey(30),
                        new MultiKey(KeyEvent.VK_J,KeyEvent.VK_K,KeyEvent.VK_U,KeyEvent.VK_I),new TimeKey(30),
                        new MultiKey(KeyEvent.VK_J,KeyEvent.VK_K,KeyEvent.VK_U,KeyEvent.VK_I),new TimeKey(30),
                        new MultiKey(KeyEvent.VK_J,KeyEvent.VK_K,KeyEvent.VK_U,KeyEvent.VK_I),new TimeKey(30),
                        new MultiKey(KeyEvent.VK_J,KeyEvent.VK_K,KeyEvent.VK_U,KeyEvent.VK_I),new TimeKey(30),
                        new MultiKey(KeyEvent.VK_J,KeyEvent.VK_K,KeyEvent.VK_U,KeyEvent.VK_I),new TimeKey(30),
                        new MultiKey(KeyEvent.VK_J,KeyEvent.VK_K,KeyEvent.VK_U,KeyEvent.VK_I),new TimeKey(30),
                        new MultiKey(KeyEvent.VK_J,KeyEvent.VK_K,KeyEvent.VK_U,KeyEvent.VK_I),new TimeKey(30),
                        new MultiKey(KeyEvent.VK_J,KeyEvent.VK_K,KeyEvent.VK_U,KeyEvent.VK_I),new TimeKey(30),
                        new MultiKey(KeyEvent.VK_J,KeyEvent.VK_K,KeyEvent.VK_U,KeyEvent.VK_I),new TimeKey(30),
                        new MultiKey(KeyEvent.VK_J,KeyEvent.VK_K,KeyEvent.VK_U,KeyEvent.VK_I),new TimeKey(30),
                        new MultiKey(KeyEvent.VK_J,KeyEvent.VK_K,KeyEvent.VK_U,KeyEvent.VK_I),new TimeKey(30),
                        new MultiKey(KeyEvent.VK_J,KeyEvent.VK_K,KeyEvent.VK_U,KeyEvent.VK_I),new TimeKey(30),
                        new MultiKey(KeyEvent.VK_J,KeyEvent.VK_K,KeyEvent.VK_U,KeyEvent.VK_I),new TimeKey(30),
                        new MultiKey(KeyEvent.VK_J,KeyEvent.VK_K,KeyEvent.VK_U,KeyEvent.VK_I),new TimeKey(30)
                        );
//                keyRobot.press(new M2RKey(), new R2LKey(), new OrderKey(KeyEvent.VK_U));
                break;
            case KeyEvent.VK_P:
                // 铁球大暴走
                keyRobot.press(new MultiKey(KeyEvent.VK_J, KeyEvent.VK_U, KeyEvent.VK_I, KeyEvent.VK_K));
                break;
            case KeyEvent.VK_N:
                // 铁球大暴走
                keyRobot.press(new OrderKey(KeyEvent.VK_J, KeyEvent.VK_J, KeyEvent.VK_J, KeyEvent.VK_J));
                break;
            case KeyEvent.VK_M:
                // 铁球大暴走
                keyRobot.press(new R2LKey(), new OrderKey(KeyEvent.VK_D, KeyEvent.VK_U));
                break;
        }
    }
}
