package cn.zj.smart.robot.defkey;

import cn.zj.smart.robot.AdRobot;
import cn.zj.smart.robot.Keys;

import java.awt.*;
import java.awt.event.KeyEvent;

public class M2RKey implements Keys,DefineKey {
    @Override
    public int[] keys() {
        return null;
    }

    @Override
    public void doRobot(boolean reverse) {
        try {
            Robot robot = new AdRobot(reverse);
            robot.setAutoWaitForIdle(true);
            robot.setAutoDelay(30);
            robot.keyPress(KeyEvent.VK_S);
            robot.keyPress(KeyEvent.VK_D);
            robot.keyRelease(KeyEvent.VK_S);
            robot.keyRelease(KeyEvent.VK_D);
        } catch (Exception e) {
        }
    }
}
