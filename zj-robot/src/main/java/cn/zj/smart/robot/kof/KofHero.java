package cn.zj.smart.robot.kof;

import com.melloware.jintellitype.HotkeyListener;

import java.awt.event.KeyEvent;

public enum KofHero {
//    Kyo_Kusanagi(KeyEvent.VK_1,new KyoHotKeyListener()),
//    Benimaru(KeyEvent.VK_1,new BenimaruHotKeyListener()),
    ;
    private int heroKey;
    private HotkeyListener hotkeyListener;

    KofHero(int heroKey, HotkeyListener hotkeyListener) {
        this.heroKey = heroKey;
        this.hotkeyListener = hotkeyListener;
    }

    public int getHeroKey() {
        return heroKey;
    }

    public HotkeyListener getHotkeyListener() {
        return hotkeyListener;
    }
}
