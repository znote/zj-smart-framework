package cn.zj.smart.robot;

public class TimeKey implements Keys{
    private long time;

    public TimeKey(long time) {
        this.time = time;
    }

    @Override
    public int[] keys() {
        return null;
    }

    public long getTime() {
        return time;
    }
}
