package cn.zj.smart.robot.kof;

import cn.zj.smart.robot.*;
import cn.zj.smart.robot.defkey.M2LKey;
import cn.zj.smart.robot.defkey.M2RKey;
import cn.zj.smart.robot.defkey.R2LKey;
import com.melloware.jintellitype.HotkeyListener;

import java.awt.event.KeyEvent;

/**
 * 二阶堂
 */
public class BenimaruHotKeyListener implements HotkeyListener {
    private KeyRobot keyRobot;

    public BenimaruHotKeyListener(KeyRobot keyRobot) {
        this.keyRobot = keyRobot;
    }

    @Override
    public void onHotKey(int i) {
        switch (i) {
            case KeyEvent.VK_0:
                // 终极奥义 大发电者
                keyRobot.press(new R2LKey(), new R2LKey(), new OrderKey(KeyEvent.VK_J));
                break;
            case KeyEvent.VK_1:
                keyRobot.press(new M2RKey(), new OrderKey(KeyEvent.VK_J));
                break;
            case KeyEvent.VK_2:
                keyRobot.press(new M2LKey(), new MultiKey(KeyEvent.VK_J));
                break;
            case KeyEvent.VK_3:
                keyRobot.press(new R2LKey(), new OrderKey(KeyEvent.VK_D, KeyEvent.VK_J));
                break;
        }
    }
}
