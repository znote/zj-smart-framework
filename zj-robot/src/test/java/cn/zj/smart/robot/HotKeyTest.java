package cn.zj.smart.robot;

import com.melloware.jintellitype.HotkeyListener;
import com.melloware.jintellitype.JIntellitype;
import org.junit.Test;

public class HotKeyTest {
    public static final int MINIMIZE_KEY_MARK = 1;
    @Test
    public void test() throws InterruptedException {
        JIntellitype.getInstance().registerHotKey(MINIMIZE_KEY_MARK, 0 , 'a');
        HotkeyListener hotkeyListener = new RunRobotHotKeyListener();
        JIntellitype.getInstance().addHotKeyListener(hotkeyListener);
        System.out.println("end!");
        Thread.sleep(100000);
    }

    public class RunRobotHotKeyListener implements HotkeyListener {
        public void onHotKey(int markCode) {
            System.out.println(" press S!!!");
        }
    }

    @Test
    public void testMethod() {
        System.out.println("===");
    }
}
