package com.zj.auth.bean;

import cn.zj.smart.util.exception.MsgCode;

/**
 * @author xi.yang
 * @create 2020-11-20 11:37
 **/
public enum AuthCode implements MsgCode {
    /**
     * account不能为空
     */
    ACCOUNT_MUST_NOT_NULL,
    /**
     * password不能为空
     */
    PASSWORD_MUST_NOT_NULL,
    /**
     * 需要登陆
     */
    NEED_LOGIN,
    /**
     * 签名错误
     */
    SIGNATURE_ERROR,
    /**
     * token错误
     */
    TOKEN_ERROR,
    /**
     * 短信验证码错误
     */
    PHONE_CODE_ERROR,
    ;
    @Override
    public String getMessage() {
        return this.name();
    }
}
