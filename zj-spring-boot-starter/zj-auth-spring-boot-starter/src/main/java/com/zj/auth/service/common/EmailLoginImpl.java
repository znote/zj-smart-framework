package com.zj.auth.service.common;

import com.zj.auth.bean.AuthCode;
import com.zj.auth.domain.Customer;
import com.zj.auth.repository.CustomerRepository;
import com.zj.auth.service.CodeService;
import cn.zj.smart.util.StringUtil;
import cn.zj.smart.util.exception.AuthException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Date;

/**
 * @author xi.yang
 * @create 2020-11-19 20:55
 **/
@Service("EMAILLoginImpl")
public class EmailLoginImpl extends CommonAuthImpl {
    @Autowired
    private CustomerRepository customerRepository;
    @Autowired
    private CodeService codeService;
    @Override
    public void validate() {
        super.validate();
        if (StringUtil.isBlank(this.loginTo.getPassword())) {
            throw new AuthException(AuthCode.PASSWORD_MUST_NOT_NULL);
        }
        // 验证验证码 TODO
        if (codeService.validate(this.loginTo)) {

        }
    }

    @Override
    public Customer getCustomer() {
        return customerRepository.findByPhone(this.loginTo.getAccount());
    }

    @Override
    public Customer registerCustomer() {
        Customer customer = new Customer();
        customer.setCreateTime(new Date());
        customer.setPhone(this.loginTo.getAccount());
        customer.setUsername(StringUtil.hiddenPhone(this.loginTo.getAccount()));
        return customerRepository.save(customer);
    }
}
