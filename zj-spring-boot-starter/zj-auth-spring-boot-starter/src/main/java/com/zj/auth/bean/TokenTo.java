package com.zj.auth.bean;

import cn.zj.smart.util.eo.PlatformType;

public class TokenTo {
    private String memberId;
    private String token;
    private String appVersion;
    private String apiVersion;
    /**
     * 设备唯一标识，重装app不会改变
     */
    private String udid;
    /**
     * md5(url+盐值+memberId+token(去掉前三位和后三位)+appVersion+udid+apiVersion+platformType+sendTime)
     */
    private String signature;
    private PlatformType platformType;
    /**
     * 发送请求的时间
     */
    private long sendTime;

    public String getMemberId() {
        return memberId;
    }

    public void setMemberId(String memberId) {
        this.memberId = memberId;
    }

    public String getToken() {
        return token;
    }

    public void setToken(String token) {
        this.token = token;
    }

    public String getAppVersion() {
        return appVersion;
    }

    public void setAppVersion(String appVersion) {
        this.appVersion = appVersion;
    }

    public String getApiVersion() {
        return apiVersion;
    }

    public void setApiVersion(String apiVersion) {
        this.apiVersion = apiVersion;
    }

    public String getUdid() {
        return udid;
    }

    public void setUdid(String udid) {
        this.udid = udid;
    }

    public String getSignature() {
        return signature;
    }

    public void setSignature(String signature) {
        this.signature = signature;
    }

    public PlatformType getPlatformType() {
        return platformType;
    }

    public void setPlatformType(PlatformType platformType) {
        this.platformType = platformType;
    }

    public long getSendTime() {
        return sendTime;
    }

    public void setSendTime(long sendTime) {
        this.sendTime = sendTime;
    }
}