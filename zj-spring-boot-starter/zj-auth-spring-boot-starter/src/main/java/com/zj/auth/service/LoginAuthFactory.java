package com.zj.auth.service;

import com.zj.auth.type.LoginType;
import com.zj.auth.type.ThirdType;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.Map;

/**
 * @author xi.yang
 * @create 2020-11-20 12:24
 **/
@Component
public class LoginAuthFactory {
    @Autowired
    private Map<String, LoginAuth> map;

    public LoginAuth getCommonLoginAuth(LoginType loginType) {
        return map.get(loginType.name() + "LoginImpl");
    }

    public LoginAuth getThirdLoginAuth(ThirdType loginType) {
        return map.get(loginType.name() + "LoginImpl");
    }
}
