package com.zj.auth.service;

import com.zj.auth.bean.LoginToInterface;
import com.zj.auth.domain.Customer;

/**
 * 抽象
 *
 * @author xi.yang
 * @create 2020-08-20 16:49
 **/
public interface AuthInterface {
    /**
     * 转换成自己的登录数据
     * @param loginToInterface
     */
    void initLoginTo(LoginToInterface loginToInterface);
    /**
     * 验证参数
     */
    void validate();

    /**
     * 获取已存在用户
     * @return
     */
    Customer getCustomer();

    /**
     * 注册新用户
     * @return
     */
    Customer registerCustomer();
}
