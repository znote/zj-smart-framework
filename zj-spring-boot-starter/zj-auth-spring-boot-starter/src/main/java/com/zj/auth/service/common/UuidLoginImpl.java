package com.zj.auth.service.common;

import com.zj.auth.domain.Customer;
import com.zj.auth.repository.CustomerRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Date;

/**
 * @author xi.yang
 * @create 2020-11-19 20:55
 **/
@Service("UUIDLoginImpl")
public class UuidLoginImpl extends CommonAuthImpl {
    @Autowired
    private CustomerRepository customerRepository;

    @Override
    public Customer getCustomer() {
        return customerRepository.findByUuid(this.loginTo.getAccount());
    }

    @Override
    public Customer registerCustomer() {
        Customer customer = new Customer();
        customer.setCreateTime(new Date());
        return customerRepository.save(customer);
    }
}
