package com.zj.auth.bean;

import com.zj.auth.type.LoginType;

import java.io.Serializable;

/**
 * 登录对象封装
 *
 * @author xi.yang
 * @create 2019-09-03 9:16
 **/
public class LoginTo implements LoginToInterface, Serializable {
    /**
     * 登录类型
     */
    private LoginType loginType;
    /**
     * 账号/邮箱
     */
    private String account;
    /**
     * 密码/code
     */
    private String password;

    public LoginType getLoginType() {
        return loginType;
    }

    public void setLoginType(LoginType loginType) {
        this.loginType = loginType;
    }

    public String getAccount() {
        return account;
    }

    public void setAccount(String account) {
        this.account = account;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }
}
