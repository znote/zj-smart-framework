package com.zj.auth.service.third;

import com.zj.auth.bean.LoginToInterface;
import com.zj.auth.bean.ThirdLoginTo;
import com.zj.auth.domain.Customer;
import com.zj.auth.service.AuthInterface;
import org.springframework.stereotype.Service;

/**
 * 统一的第三方登陆
 * @author xi.yang
 * @create 2020-11-20 12:08
 **/
@Service
public abstract class ThirdAuthImpl implements AuthInterface {
    protected ThirdLoginTo thirdLoginTo;

    @Override
    public void initLoginTo(LoginToInterface loginToInterface) {
        this.thirdLoginTo = (ThirdLoginTo) loginToInterface;
    }

    @Override
    public Customer getCustomer() {
        return null;
    }

    @Override
    public Customer registerCustomer() {
        return null;
    }
}
