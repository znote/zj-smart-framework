package com.zj.auth.bean;

import com.zj.auth.type.ThirdType;
/**
 * 三方登录对象封装
 *
 * @author xi.yang
 * @create 2019-09-03 9:16
 **/
public class ThirdLoginTo implements LoginToInterface {
    private ThirdType loginType;
    private String token;

    public ThirdType getLoginType() {
        return loginType;
    }

    public void setLoginType(ThirdType loginType) {
        this.loginType = loginType;
    }

    public String getToken() {
        return token;
    }

    public void setToken(String token) {
        this.token = token;
    }
}
