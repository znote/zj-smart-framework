package com.zj.auth.config;

import org.springframework.boot.context.properties.ConfigurationProperties;

/**
 * @author xi.yang
 * @create 2020-03-30 12:16
 **/
@ConfigurationProperties(prefix = "zj-auth")
public class AuthConfig {
    /**
     * tokenHeaderName
     */
    private String tokenHeaderName = "zj-auth-token";
    /**
     * loginRedisKey
     * 默认：zj-auth-login-redis
     */
    private String loginRedisKey = "zj-auth-login-redis";
    /**
     * 登陆token过期时间（分钟）
     * 默认60分钟
     */
    private long tokenRedisExpire = 60L;
    /**
     * api版本号
     */
    private String apiVersion = "1.0.0";
    /**
     * 加密盐值
     */
    private String authSalt = "testouyangxi";
    /**
     * app名，验证码签名用
     */
    private String appName;

    public String getTokenHeaderName() {
        return tokenHeaderName;
    }

    public void setTokenHeaderName(String tokenHeaderName) {
        this.tokenHeaderName = tokenHeaderName;
    }

    public String getLoginRedisKey() {
        return loginRedisKey;
    }

    public void setLoginRedisKey(String loginRedisKey) {
        this.loginRedisKey = loginRedisKey;
    }

    public long getTokenRedisExpire() {
        return tokenRedisExpire;
    }

    public void setTokenRedisExpire(long tokenRedisExpire) {
        this.tokenRedisExpire = tokenRedisExpire;
    }

    public String getApiVersion() {
        return apiVersion;
    }

    public void setApiVersion(String apiVersion) {
        this.apiVersion = apiVersion;
    }

    public String getAuthSalt() {
        return authSalt;
    }

    public String getAppName() {
        return appName;
    }

    public void setAppName(String appName) {
        this.appName = appName;
    }

    public void setAuthSalt(String authSalt) {
        this.authSalt = authSalt;
    }
}
