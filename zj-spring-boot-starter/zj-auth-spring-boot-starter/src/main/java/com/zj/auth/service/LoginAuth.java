package com.zj.auth.service;

import com.zj.auth.bean.LoginToInterface;
import com.zj.auth.bean.LoginToken;

/**
 * 抽象
 *
 * @author xi.yang
 * @create 2020-08-20 16:49
 **/
public interface LoginAuth {
    /**
     * 统一的登陆接口
     * @param loginToInterface
     * @return
     */
    LoginToken login(LoginToInterface loginToInterface);
}
