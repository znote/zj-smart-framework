package com.zj.auth;

/**
 * 手机发送验证码接口
 * @author xi.yang
 * @create 2020-12-21 10:43
 **/
public interface SendPhoneCode {
    boolean sendCode(String phoneNum, String code);
}
