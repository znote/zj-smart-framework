package com.zj.auth.cache;

import com.zj.auth.config.AuthConfig;
import cn.zj.smart.util.StringUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.StringRedisTemplate;
import org.springframework.stereotype.Component;

import java.util.concurrent.TimeUnit;

/**
 * @author xi.yang
 * @create 2020-11-20 15:04
 **/
@Component
public class RedisUtil {
    @Autowired
    private StringRedisTemplate stringRedisTemplate;
    @Autowired
    private AuthConfig authConfig;

    /**
     * 缓存token
     * @param customerId
     * @param token
     */
    public void cacheCustomerToken(String customerId,String token) {
        stringRedisTemplate.opsForValue().set(getKey(token), customerId, authConfig.getTokenRedisExpire(), TimeUnit.MINUTES);
    }

    public String getCustomerId(String token) {
        return stringRedisTemplate.opsForValue().get(getKey(token));
    }

    private String getKey(String token) {
        return authConfig.getLoginRedisKey() + ":" + token;
    }

    private String getPoneCodeKey(String phoneNum) {
        return "phone_code_cache:" + phoneNum;
    }

    public void cachePhoneCode(String phoneNum,String code) {
        stringRedisTemplate.opsForValue().set(getPoneCodeKey(phoneNum), code, 5, TimeUnit.MINUTES);
    }

    public boolean validatePhoneCode(String phoneNum,String code) {
        String exist = stringRedisTemplate.opsForValue().get(getPoneCodeKey(phoneNum));
        if (StringUtil.isBlank(exist)) {
            return false;
        }
        return exist.equals(code);
    }
}