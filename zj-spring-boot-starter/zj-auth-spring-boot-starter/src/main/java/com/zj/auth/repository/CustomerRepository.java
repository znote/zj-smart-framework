package com.zj.auth.repository;

import com.zj.auth.domain.Customer;
import org.springframework.data.mongodb.repository.MongoRepository;

public interface CustomerRepository extends MongoRepository<Customer, String> {
    Customer findByUuid(String uuid);
    Customer findByPhone(String phone);
    Customer findByUsername(String username);
}
