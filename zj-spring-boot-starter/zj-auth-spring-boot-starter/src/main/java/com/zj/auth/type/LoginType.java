package com.zj.auth.type;

/**
 * 登录类型
 *
 * @author xi.yang
 * @create 2019-09-03 9:19
 **/
public enum LoginType {
    /**
     * 设备唯一码
     */
    UUID,
    /**
     * 邮箱
     */
    EMAIL,
    /**
     * 手机
     */
    PHONE,
    /**
     * 账户密码登录
     */
    ACCOUNT
}
