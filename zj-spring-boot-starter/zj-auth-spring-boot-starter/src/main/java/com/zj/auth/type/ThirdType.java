package com.zj.auth.type;

/**
 * 三方登陆类型
 *
 * @author xi.yang
 * @create 2020-11-19 18:29
 **/
public enum ThirdType {
    /**
     * facebook
     */
    FACEBOOK,
    /**
     * google
     */
    GOOGLE;
}
