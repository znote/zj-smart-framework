package com.zj.auth.bean;

import java.io.Serializable;

/**
 * 登录凭证
 * @author xi.yang
 * @create 2019-09-03 9:27
 **/
public class LoginToken implements Serializable {
    /**
     * 用户id
     */
    private String customerId;
    /**
     * 注入token的header名
     */
    private String tokenHeader;
    /**
     * token值
     */
    private String token;
    /**
     * 过期时间
     */
    private long expire;

    public String getCustomerId() {
        return customerId;
    }

    public void setCustomerId(String customerId) {
        this.customerId = customerId;
    }

    public String getTokenHeader() {
        return tokenHeader;
    }

    public void setTokenHeader(String tokenHeader) {
        this.tokenHeader = tokenHeader;
    }

    public String getToken() {
        return token;
    }

    public void setToken(String token) {
        this.token = token;
    }

    public long getExpire() {
        return expire;
    }

    public void setExpire(long expire) {
        this.expire = expire;
    }

    public static LoginTokenBuilder builder() {
        return new LoginTokenBuilder();
    }

    public static final class LoginTokenBuilder {
        private String customerId;
        private String tokenHeader;
        private String token;
        private long expire;

        private LoginTokenBuilder() {
        }

        public LoginTokenBuilder customerId(String customerId) {
            this.customerId = customerId;
            return this;
        }

        public LoginTokenBuilder tokenHeader(String tokenHeader) {
            this.tokenHeader = tokenHeader;
            return this;
        }

        public LoginTokenBuilder token(String token) {
            this.token = token;
            return this;
        }

        public LoginTokenBuilder expire(long expire) {
            this.expire = expire;
            return this;
        }

        public LoginToken build() {
            LoginToken loginToken = new LoginToken();
            loginToken.setCustomerId(customerId);
            loginToken.setTokenHeader(tokenHeader);
            loginToken.setToken(token);
            loginToken.setExpire(expire);
            return loginToken;
        }
    }
}
