package com.zj.auth.domain;

import com.zj.auth.type.ThirdType;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

import java.io.Serializable;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

/**
 * 用户信息
 *
 * @author xi.yang
 * @create 2020-11-04 12:29
 **/
@Document(collection = "t_customer")
public class Customer implements Serializable {
    /**
     * 唯一id
     */
    @Id
    private String id;
    /**
     * 设备唯一id
     */
    private String uuid;
    /**
     * 用户名
     */
    private String username;
    /**
     * 手机
     */
    private String phone;
    /**
     * 邮箱
     */
    private String email;
    /**
     * 加密后的密码
     */
    private String pwdStr;
    /**
     * 第三方登录openId
     */
    private Map<ThirdType, String> thirdOpenIds = new HashMap<>();
    /**
     * 创建时间
     */
    private Date createTime;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getUuid() {
        return uuid;
    }

    public void setUuid(String uuid) {
        this.uuid = uuid;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getPwdStr() {
        return pwdStr;
    }

    public void setPwdStr(String pwdStr) {
        this.pwdStr = pwdStr;
    }

    public Map<ThirdType, String> getThirdOpenIds() {
        return thirdOpenIds;
    }

    public void setThirdOpenIds(Map<ThirdType, String> thirdOpenIds) {
        this.thirdOpenIds = thirdOpenIds;
    }

    public Date getCreateTime() {
        return createTime;
    }

    public void setCreateTime(Date createTime) {
        this.createTime = createTime;
    }
}
