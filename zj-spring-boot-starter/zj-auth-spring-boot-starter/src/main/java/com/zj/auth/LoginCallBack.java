package com.zj.auth;

/**
 * 登录接入接口
 * @author xi.yang
 * @create 2020-11-04 17:59
 **/
public interface LoginCallBack {
    /**
     * 登录成功的回调
     * @param customerId
     */
    void loginSuccess(String customerId);

    /**
     * 创建新用户的回调
     * @param customerId
     */
    void createNewCustomerSuccess(String customerId);
}
