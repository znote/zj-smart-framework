package com.zj.auth.util;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;
import java.util.stream.IntStream;

/**
 * @author xi.yang(xi.yang@downjoy.com)
 * @create 2018-05-21 下午 3:12
 **/
public class CodeUtil {
    private static List<Integer> nums = new ArrayList<>(1000);
    /**
     * 获取6位数验证码
     *
     * @return 验证码
     */
    public static String randomCode() {
        if (nums.size() <= 10) {
            IntStream intStream = new Random().ints(1000, 100000, 1000000);
            intStream.forEach(i -> nums.add(i));
        }
        return String.valueOf(nums.remove(0));
    }

}
