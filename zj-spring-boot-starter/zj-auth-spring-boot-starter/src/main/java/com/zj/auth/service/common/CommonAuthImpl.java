package com.zj.auth.service.common;

import com.zj.auth.bean.AuthCode;
import com.zj.auth.bean.LoginTo;
import com.zj.auth.bean.LoginToInterface;
import com.zj.auth.service.AuthImpl;
import cn.zj.smart.util.StringUtil;
import cn.zj.smart.util.exception.AuthException;

/**
 * @author xi.yang
 * @create 2020-11-20 11:47
 **/
public abstract class CommonAuthImpl extends AuthImpl {
    protected LoginTo loginTo;

    @Override
    public void initLoginTo(LoginToInterface loginToInterface) {
        this.loginTo = (LoginTo) loginToInterface;
    }

    @Override
    public void validate() {
        if (StringUtil.isBlank(loginTo.getAccount())) {
            throw new AuthException(AuthCode.ACCOUNT_MUST_NOT_NULL);
        }
    }
}
