package cn.zj.smart.util.bean;

import java.net.Proxy;

/**
 * 代理类
 *
 * @author xi.yang
 * @create 2020-10-19 20:26
 **/
public class HttpConfig {
    private static final HttpConfig proxyConfig = new HttpConfig();
    private int timeout = 3000;
    private Proxy proxy;

    private HttpConfig() {
    }

    public static HttpConfig getInstance(Proxy proxy) {
        proxyConfig.proxy = proxy;
        return proxyConfig;
    }

    public static HttpConfig getInstance(int timeout, Proxy proxy) {
        proxyConfig.timeout = timeout;
        proxyConfig.proxy = proxy;
        return proxyConfig;
    }

    public int getTimeout() {
        return timeout;
    }

    public Proxy getProxy() {
        return proxy;
    }
}
