package cn.zj.smart.util;

import cn.zj.smart.util.eo.DatePattern;
import com.alibaba.fastjson.JSON;

import java.util.List;

/**
 * @author xi.yang
 * @create 2019-05-13 9:27
 **/
public class JsonUtil {
    public static String toString(Object o) {
        return JSON.toJSONStringWithDateFormat(o, DatePattern.yyyyMMdd_HHmmss.getPattern());
    }

    public static  <T> T parse(String jsonStr, Class<T> clazz) {
        return JSON.parseObject(jsonStr, clazz);
    }

    public static  <T> List<T> parseList(String jsonStr, Class<T> clazz) {
        return JSON.parseArray(jsonStr, clazz);
    }
}
