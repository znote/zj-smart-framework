package cn.zj.smart.util;

import cn.zj.smart.util.eo.RsaSign;

import java.nio.charset.StandardCharsets;
import java.security.KeyFactory;
import java.security.PublicKey;
import java.security.Signature;
import java.security.spec.X509EncodedKeySpec;

/**
 * 各种签名和编码相关的方法
 *
 * @author xi.yang
 * @create 2020-10-20 12:11
 **/
public class SecurityUtil {
    /**
     * RSA验签名校验
     *
     * @param content    待签名数据
     * @param public_key 公钥
     * @param sign       签名值
     * @return 布尔值
     */
    public static boolean verifyRsa(RsaSign rsaSign, String content, String public_key, String sign) {
        try {
            // 获取公钥密钥字节数组
            byte[] encodedKey = Base64Util.decode(public_key);
            // 获得密钥规范
            X509EncodedKeySpec keySpec = new X509EncodedKeySpec(encodedKey);
            // 实例化密钥工厂，并指定RSA算法
            KeyFactory keyFactory = KeyFactory.getInstance("RSA");
            // 生成公钥
            PublicKey pubKey = keyFactory.generatePublic(keySpec);
            // 实例化Signature对象
            Signature signature = Signature.getInstance(rsaSign.name());
            // 初始化用于验签操作的Signature对象
            signature.initVerify(pubKey);
            // 使用指定的 byte 数组更新要验签的数据
            signature.update(content.getBytes(StandardCharsets.UTF_8));
            // 验签
            return signature.verify(Base64Util.decode(sign));
        } catch (Exception e) {
            e.printStackTrace();
        }
        return false;
    }
}
