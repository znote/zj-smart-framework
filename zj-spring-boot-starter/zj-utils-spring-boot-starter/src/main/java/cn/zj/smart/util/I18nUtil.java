package cn.zj.smart.util;

import cn.zj.smart.util.eo.LangType;
import org.springframework.context.i18n.LocaleContextHolder;
import org.springframework.context.support.MessageSourceAccessor;
import org.springframework.context.support.ReloadableResourceBundleMessageSource;
import org.springframework.core.io.Resource;
import org.springframework.core.io.support.PathMatchingResourcePatternResolver;
import org.springframework.core.io.support.ResourcePatternResolver;

import java.io.IOException;

/**
 * @Description: 多语言国际化消息工具类
 * @Author: junqiang.lu
 * @Date: 2019/1/24
 */
public class I18nUtil {

    private static MessageSourceAccessor accessor;
    private static final String PATH_PARENT = "classpath:i18n/messages_";
    private static final String SUFFIX = ".properties";
    private static ResourcePatternResolver resourcePatternResolver = new PathMatchingResourcePatternResolver();

    private I18nUtil(){
    }

    /**
     * 初始化资源文件的存储器
     * 加载指定语言配置文件
     *
     * @param language 语言类型(文件名即为语言类型,eg: en_us 表明使用 美式英文 语言配置)
     */
    private static void initMessageSourceAccessor(String language) throws IOException {
        /**
         * 获取配置文件名
         */
        Resource resource = resourcePatternResolver.getResource(PATH_PARENT + language + SUFFIX);
        String fileName = resource.getURL().toString();
        int lastIndex = fileName.lastIndexOf(".");
        String baseName = fileName.substring(0,lastIndex);
        /**
         * 读取配置文件
         */
        ReloadableResourceBundleMessageSource reloadableResourceBundleMessageSource = new ReloadableResourceBundleMessageSource();
        reloadableResourceBundleMessageSource.setBasename(baseName);
        reloadableResourceBundleMessageSource.setCacheSeconds(5);
        reloadableResourceBundleMessageSource.setDefaultEncoding("UTF-8");
        accessor = new MessageSourceAccessor(reloadableResourceBundleMessageSource);
    }

    /**
     * 获取一条语言配置信息
     *
     * @param langType       语言类型
     * @param message        配置信息属性名,eg: api.response.code.user.signUp
     * @param defaultMessage 默认信息,当无法从配置文件中读取到对应的配置信息时返回该信息
     * @return
     * @throws IOException
     */
    public static String getMessage(LangType langType, String message, String defaultMessage) {
        try {
            initMessageSourceAccessor(langType.getLanguage());
            return accessor.getMessage(message, defaultMessage, LocaleContextHolder.getLocale());
        } catch (Exception e) {
            e.printStackTrace();
            return defaultMessage;
        }
    }

    public static String getMessage(String message, Object... objects) {
        String result = getMessage(LangType.PORTUGUESE, message, objects);
        if (StringUtil.isBlank(result)) {
            return message;
        }
        return result;
    }

    public static String getMessage(LangType langType, String message, Object... objects) {
        String result = getMessage(langType, message, "");
        if (StringUtil.isBlank(result)) {
            return message;
        }
        for (int i = 0; i < objects.length; i++) {
            result = result.replace("{" + i + "}", "" + objects[i]);
        }
        return result;
    }

    public static String getMessage(LangType langType, Enum e) {
        final String message = e.getClass().getName() + "." + e.name();
        String result = getMessage(langType, message);
        if (StringUtil.isBlank(result)) {
            return e.name();
        }
        return result;
    }
}