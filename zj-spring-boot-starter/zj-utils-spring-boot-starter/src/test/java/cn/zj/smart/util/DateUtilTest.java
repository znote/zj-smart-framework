package cn.zj.smart.util;

import cn.zj.smart.util.eo.DateDtf;
import cn.zj.smart.util.eo.DatePattern;
import org.junit.Test;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.ZoneId;
import java.util.Date;

/**
 * @author xi.yang
 * @create 2020-11-17 16:26
 **/
public class DateUtilTest {
    @Test
    public void parseDateTest() {
        Date date = DateUtil.parseDate("2020-11", DatePattern.yyyyMM);
        System.out.println("=======" + date);
    }

    @Test
    public void isSameWeekTest() {
        System.out.println("==="+DateUtil.isSameWeek(LocalDate.parse("2020-12-28"),LocalDate.parse("2021-01-02")));
    }

    @Test
    public void testTimeLong() {
        System.out.println(DateUtil.curLocalDateTime() + "===" +DateUtil.localDateTime2Long(DateUtil.curLocalDateTime()));
        DateUtil.initZone(ZoneId.of("Europe/Paris"));
        System.out.println(DateUtil.curLocalDateTime() + "===" +DateUtil.localDateTime2Long(DateUtil.curLocalDateTime()));
        DateUtil.initZone(ZoneId.of("America/Los_Angeles"));
        System.out.println(DateUtil.curLocalDateTime() + "===" +DateUtil.localDateTime2Long(DateUtil.curLocalDateTime()));
    }

    @Test
    public void stringTransTest() {
        for (DateDtf value : DateDtf.values()) {
            testDateTimeFormatter(value);
        }
    }

    private void testDateTimeFormatter(DateDtf dateDtf) {
        try {
            long cur = DateUtil.currentTimeMillis();
            String str = DateUtil.long2String(cur, dateDtf);
            LocalDateTime after = DateUtil.string2LocalDateTime(str, dateDtf);
            System.out.println(dateDtf+"======="+after+"==="+str);
        } catch (Exception e) {
            System.out.println("error==="+dateDtf);
        }
    }
}
