package cn.zj.smart.util;

import cn.zj.smart.util.eo.Gender;
import org.junit.Test;

/**
 * @author xi.yang
 * @create 2020-10-20 11:26
 **/
public class EnumUtilTest {
    @Test
    public void getIdEnumTest() {
        Gender gender = EnumUtil.getIdEnum(Gender.class, 6);
        System.out.println("======="+gender);
        gender = EnumUtil.getIdEnum(Gender.class, 1);
        gender = EnumUtil.getIdEnum(Gender.class, 2);
        gender = EnumUtil.getIdEnum(Gender.class, 0);
        gender = EnumUtil.getIdEnum(Gender.class, 7);
    }
}
