## 各种工具/框架的扩展包装starter
- [x] [zj-utils-spring-boot-starter](./zj-utils-spring-boot-starter)：常用的工具类和通用模型
- [x] [zj-swagger-spring-boot-starter](./zj-swagger-spring-boot-starter)：swagger扩展，直接用注释来生成文档
- [x] [zj-mongodb-springboot-starter](./zj-mongodb-springboot-starter)：mongodb扩展

## 使用方法
1. clone项目到本地
```
git clone https://gitee.com/znote/zj-spring-boot-starter.git
```
2. mvn打包到本地仓库
```
mvn clean install
```
3. 在项目中通过maven引入
```
		<dependency>
			<groupId>com.zj.springboot.starter</groupId>
			<artifactId>zj-spring-boot-starter</artifactId>
			<version>1.0.0</version>
		</dependency>
```
如果只用某个子项目，就只引入子项目即可。

4. 配置相关子项目属性

