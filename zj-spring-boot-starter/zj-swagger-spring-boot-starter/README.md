## 项目思路文档
> https://www.jianshu.com/p/a21063cb89f7

## 使用方法
1. clone项目到本地
```
git clone https://gitee.com/znote/zj-swagger-spring-boot-starter.git
```
2. mvn打包到本地仓库
```
mvn clean install
```
3. 在项目中通过maven引入
```
		<dependency>
			<groupId>com.zj.springboot.starter</groupId>
			<artifactId>zj-swagger-spring-boot-starter</artifactId>
			<version>1.0.0</version>
		</dependency>
```
4. 配置springboot属性
```
swagger-config.doc-project-path=C:\\Users\\d.cn\\Desktop\\proj\\java_project.obj
```
这个属性开发环境和发布环境可以不一样，在本地调试好后发布到服务器上的同时要拷贝这个文件到服务器

5. 本地把项目跑起来测试文档没问题后，将生成的java_project.obj拷贝到服务器配置的具体路径

6. 再启动服务时，文档就和本地一样了