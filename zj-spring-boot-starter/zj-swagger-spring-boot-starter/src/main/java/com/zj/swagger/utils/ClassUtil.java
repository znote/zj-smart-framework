package com.zj.swagger.utils;

import java.lang.reflect.Field;

/**
 * @author xi.yang
 * @create 2020-03-30 12:47
 **/
public class ClassUtil {
    public static Field getDeclaredField(final Class<?> cls, final String fieldName, final boolean forceAccess) {
        try {
            // only consider the specified class by using getDeclaredField()
            final Field field = cls.getDeclaredField(fieldName);
            if (forceAccess) {
                field.setAccessible(true);
            } else {
                return null;
            }
            return field;
        } catch (final NoSuchFieldException e) { // NOPMD
            // ignore
        }
        return null;
    }
}
