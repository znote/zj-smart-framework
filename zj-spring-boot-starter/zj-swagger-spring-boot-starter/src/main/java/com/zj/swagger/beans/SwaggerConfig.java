package com.zj.swagger.beans;

import lombok.Data;
import org.springframework.boot.context.properties.ConfigurationProperties;

/**
 * @author xi.yang
 * @create 2020-03-30 12:16
 **/
@Data
@ConfigurationProperties(prefix = "swagger-config")
public class SwaggerConfig {
    private String docProjectPath;
}
