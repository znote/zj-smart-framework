package cn.dagteam.springboot.mongodb.starter.entity;

import lombok.Data;
import lombok.EqualsAndHashCode;

@Data
@EqualsAndHashCode(callSuper = true)
public abstract class LogicDeleteEntity extends LifeCycleEntity {
	
	private static final long serialVersionUID = 1809163138043537126L;

    /**
     * 删除标记（正常；删除；审核）
     */
    protected Boolean delFlag = Boolean.FALSE;
}
