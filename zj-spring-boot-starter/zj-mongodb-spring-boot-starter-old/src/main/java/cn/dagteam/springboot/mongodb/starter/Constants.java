package cn.dagteam.springboot.mongodb.starter;

/**
 * @author gaofeng
 */
public interface Constants {

    long VERSION = 1L;

    int DEFAULT_PAGE_NO = 1;

    int DEFAULT_PAGE_SIZE = 20;

    String ARRAY_STRING_DELIMITER = ",";
    
    String FILTER_OR_OPERATOR = "_OR_";
}