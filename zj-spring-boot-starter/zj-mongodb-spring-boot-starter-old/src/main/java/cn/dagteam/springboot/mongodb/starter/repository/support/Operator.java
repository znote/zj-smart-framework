package cn.dagteam.springboot.mongodb.starter.repository.support;

/**
 * @author gaofeng
 */
public enum Operator {
    /**
     *
     */
    EQ("is"),
    /**
     *
     */
    NEQ("ne"),
    /**
     *
     */
    LIKE("regex", "^.*{0}.*$"),
    /**
     *
     */
    LLIKE("regex", "^.*{0}$"),
    /**
     *
     */
    RLIKE("regex", "^{0}.*$"),
    /**
     *
     */
    GT("gt"),
    /**
     *
     */
    LT("lt"),
    /**
     *
     */
    GTE("gte"),
    /**
     *
     */
    LTE("lte"),
    /**
     *
     */
    IN("in"),
    /**
     *
     */
    NOTIN("nin"),
    /**
     *
     */
    EXISTS("exists", "true"),
    /**
     *
     */
    NEXISTS("exists", "false");

    private String methodName;

    private String pattern;

    Operator(String methodName) {
        this.methodName = methodName;
    }

    Operator(String methodName, String pattern) {
        this.methodName = methodName;
        this.pattern = pattern;
    }

    public String getMethodName() {
        return methodName;
    }

    public String getPattern() {
        return pattern;
    }
}