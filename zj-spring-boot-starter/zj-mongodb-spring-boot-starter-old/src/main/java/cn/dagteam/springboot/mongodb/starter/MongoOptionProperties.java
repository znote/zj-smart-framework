package cn.dagteam.springboot.mongodb.starter;

import lombok.Data;
import org.springframework.boot.context.properties.ConfigurationProperties;

/**
 * @author d7
 * @author gaofeng
 */
@Data
@ConfigurationProperties(prefix = "spring.data.mongodb.option")
public class MongoOptionProperties {

    private boolean enabled = Boolean.FALSE;
    /**
     * Sets the minimum number of connections per host.
     */
    private Integer minConnectionPerHost = 0;

    /**
     * Sets the maximum number of connections per host.
     */
    private Integer maxConnectionPerHost = 100;

    /**
     * Sets the multiplier for number of threads allowed to block waiting for a connection.
     */
    private Integer threadsAllowedToBlockForConnectionMultiplier = 5;

    /**
     * Sets the server selection timeout in milliseconds, which defines how long the driver will wait for server selection to succeed before throwing an exception.
     */
    private Integer serverSelectionTimeout = 30000;

    /**
     * Sets the maximum time that a thread will block waiting for a connection.
     */
    private Integer maxWaitTime = 120000;

    /**
     * Sets the maximum idle time for a pooled connection.
     */
    private Integer maxConnectionIdleTime = 0;

    /**
     * Sets the maximum life time for a pooled connection.
     */
    private Integer maxConnectionLifeTime = 0;

    /**
     * Sets the connection timeout.
     */
    private Integer connectTimeout = 10000;

    /**
     * Sets the socket timeout.
     */
    private Integer socketTimeout = 0;

//    private Boolean socketKeepAlive = true; // 默认为true，设false已经不被推荐了

    /**
     * Sets whether to use SSL
     */
    private Boolean sslEnabled = false;

    /**
     * Define whether invalid host names should be allowed. Defaults to false. Take care before setting this to true, as it makes the application susceptible to man-in-the-middle attacks.
     */
    private Boolean sslInvalidHostNameAllowed = false;

    /**
     * Sets whether JMX beans registered by the driver should always be MBeans, regardless of whether the VM is Java 6 or greater. If false, the driver will use MXBeans if the VM is Java 6 or greater, and use MBeans if the VM is Java 5.
     */
    private Boolean alwaysUseMBeans = false;

    /**
     * Sets the heartbeat frequency. This is the frequency that the driver will attempt to determine the current state of each server in the cluster. The default value is 10,000 milliseconds
     */
    private Integer heartbeatFrequency = 10000;

    /**
     * Sets the minimum number of connections per host.
     */
    private Integer minHeartbeatFrequency = 500;

    /**
     * Sets the connect timeout for connections used for the cluster heartbeat.
     */
    private Integer heartbeatConnectTimeout = 20000;

    /**
     * Sets the socket timeout for connections used for the cluster heartbeat.
     */
    private Integer heartbeatSocketTimeout = 20000;

    /**
     * Sets the local threshold.
     */
    private Integer localThreshold = 15;
}
