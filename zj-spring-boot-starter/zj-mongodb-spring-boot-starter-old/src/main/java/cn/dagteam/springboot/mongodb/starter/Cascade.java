package cn.dagteam.springboot.mongodb.starter;

import java.lang.annotation.Documented;
import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

import org.springframework.data.annotation.Reference;

@Documented
@Retention(RetentionPolicy.RUNTIME)
@Target({ ElementType.FIELD })
@Reference
public @interface Cascade {

	CascadeType[] value() default CascadeType.ALL;
	
	public enum CascadeType {
		SAVE, DELETE, ALL;
	}
}
