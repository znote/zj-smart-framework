package cn.dagteam.springboot.mongodb.starter.entity;

import java.util.Date;

import org.springframework.data.annotation.CreatedBy;
import org.springframework.data.annotation.CreatedDate;
import org.springframework.data.annotation.LastModifiedBy;
import org.springframework.data.annotation.LastModifiedDate;

import lombok.Data;
import lombok.EqualsAndHashCode;

@Data
@EqualsAndHashCode(callSuper = true)
public abstract class LifeCycleEntity extends IdEntity {

	private static final long serialVersionUID = 1809163138043537126L;

	/**
	 * 创建日期
	 */
	@CreatedDate
	protected Date createDate;
	
	/**
     * 更新日期
     */
    @LastModifiedDate
    protected Date lastModifiedDate;
    
    @CreatedBy
    protected String createdBy;
    
    @LastModifiedBy
    protected String lastModifiedBy;

}
