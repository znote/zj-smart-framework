package cn.dagteam.springboot.mongodb.starter.repository;

import java.util.List;
import java.util.Optional;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.data.repository.NoRepositoryBean;

import cn.dagteam.springboot.mongodb.starter.repository.support.QueryFilter;

/**
 * @author gaofeng
 * @param <T>
 * @param <ID>
 */
@NoRepositoryBean
public interface MongoDynamicSearchRepository<T, ID> extends MongoRepository<T, ID> {

	Class<T> getEntityClass();

	/**
	 * 查询单一记录
	 */
	Optional<T> findOne(String fieldName, Object value);

	/**
	 * 查询单一记录
	 */
	Optional<T> findOne(List<QueryFilter> filters);

	/**
	 * 查询列表记录
	 */
	List<T> findList(String fieldName, Object value);

	/**
	 * 查询列表记录，排序
	 */
	List<T> findList(String fieldName, Object value, Sort sort);

	/**
	 * 查询列表记录
	 */
	List<T> findList(List<QueryFilter> filters);

	/**
	 * 查询列表记录，排序
	 */
	List<T> findList(List<QueryFilter> filters, Sort sort);

	/**
	 * 分页查询
	 */
	Page<T> findPage(List<QueryFilter> filters, int pageNo, int pageSize, Sort sort);

	/**
	 * 分页查询
	 */
	Page<T> findPage(List<QueryFilter> filters, Pageable pageable);

	/**
	 * 统计数量
	 * 
	 * @param filters
	 * @return
	 */
	int count(List<QueryFilter> filters);

	/**
	 * 判断是否存在
	 * 
	 * @param filters
	 * @return
	 */
	boolean exists(List<QueryFilter> filters);

}