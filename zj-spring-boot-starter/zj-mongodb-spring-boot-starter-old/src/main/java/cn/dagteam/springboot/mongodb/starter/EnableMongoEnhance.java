package cn.dagteam.springboot.mongodb.starter;

import org.springframework.context.annotation.Import;

import java.lang.annotation.*;

/**
 * @author gaofeng
 */
@Target({ElementType.TYPE})
@Retention(RetentionPolicy.RUNTIME)
@Documented
@Inherited
@Import({MongoEnhanceAutoConfiguration.class})
public @interface EnableMongoEnhance {}