package cn.dagteam.springboot.mongodb.starter.entity;

import java.io.Serializable;

import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;
import org.springframework.data.annotation.Id;

import cn.dagteam.springboot.mongodb.starter.Constants;
import lombok.Data;
import lombok.EqualsAndHashCode;

/**
 * @author gaofeng
 * @author d7
 */
@Data
@EqualsAndHashCode(of = "id")
public class IdEntity implements Serializable {

    private static final long serialVersionUID = Constants.VERSION;

    @Id
    private String id;

	@Override
	public String toString() {
		return ToStringBuilder.reflectionToString(this, ToStringStyle.SIMPLE_STYLE);
	}

}
