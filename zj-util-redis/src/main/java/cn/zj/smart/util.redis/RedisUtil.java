//package cn.zj.smart.util.redis;
//
//import com.google.gson.JsonNull;
//import com.google.gson.JsonParser;
//import redis.clients.jedis.*;
//import redis.clients.jedis.providers.PooledConnectionProvider;
//import redis.clients.jedis.search.Document;
//import redis.clients.jedis.search.Query;
//import redis.clients.jedis.search.SearchResult;
//
//import java.util.List;
//
//
///**
// * https://developer.redis.com/howtos/redisjson/using-java/
// * @author xi.yang
// * @create 2022-01-05 16:35
// **/
//public class RedisUtil {
//    public static void main(String[] args) {
//        HostAndPort config = new HostAndPort("redis-19484.c100.us-east-1-4.ec2.cloud.redislabs.com", 19484);
//        JedisClientConfig clientConfig = DefaultJedisClientConfig.builder()
//                .user("ouyangxi")
//                .password("QmH#!fa;Pzy:[|7!&lt;~0)C0bXFZj&gt;RvR")
//                .build();
//
//        PooledConnectionProvider provider = new PooledConnectionProvider(config, clientConfig);
//        // FIXME: 2022/1/6 文档是下面这个，不知道哪里去import
////        PooledJedisConnectionProvider provider = new PooledJedisConnectionProvider(config);
//
//        JedisSocketFactory jedisSocketFactory = new DefaultJedisSocketFactory(config, clientConfig);
//        UnifiedJedis client = new UnifiedJedis(provider);
////        Player player = new Player("ouyang","ouayngxi");
////        client.jsonSet(Player.class.getSimpleName() + ":" + 1, player);
//
//
//        client.set("testouyang", "jljflasjdlfkls");
//        System.out.println(client.get("testouyang"));
//        Student maya = new Student("Maya", "Jayavant");
//        // FIXME: 2022/1/6 这里会报错： expected value at line 1 column 1
//        client.jsonSet("student:111", maya);
////
////        Student oliwia = new Student("Oliwia", "Jagoda");
////        client.jsonSet("student:112", oliwia);
//
//        // 创建索引
////        Schema schema = new Schema().addTextField("$.firstName", 1.0).addTextField("$" +
////                ".lastName", 1.0);
////        IndexDefinition rule = new IndexDefinition(IndexDefinition.Type.JSON)
////                .setPrefixes(new String[]{"student:"});
////        client.ftCreate("student-index",
////                IndexOptions.defaultOptions().setDefinition(rule),
////                schema);
//
////        Query q = new Query("@\\$\\" + ".firstName:maya*");
////        SearchResult mayaSearch = client.ftSearch("student-index", q);
////
////        List<Document> docs = mayaSearch.getDocuments();
////        for (Document doc : docs) {
////            System.out.println(doc);
////        }
//    }
//
//
//}
