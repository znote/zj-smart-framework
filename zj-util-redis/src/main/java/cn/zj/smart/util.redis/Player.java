package cn.zj.smart.util.redis;

import java.io.Serializable;

/**
 * 单张扑克牌
 *
 * @author xi.yang
 * @create 2019-07-24 14:49
 **/
public class Player {
    public static void main(String[] args) {
        int total = 0;
        for (int i = 1; i <= 30; i++) {
            total += i;
        }
        System.out.println(total);
    }
    private String firstName;
    private String secondName;

    public Player(String firstName, String secondName) {
        this.firstName = firstName;
        this.secondName = secondName;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getSecondName() {
        return secondName;
    }

    public void setSecondName(String secondName) {
        this.secondName = secondName;
    }
}
