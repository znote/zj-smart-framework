package com.zj.gencode.type;

/**
 * 常量
 *
 * @author xi.yang
 * @create 2018-12-14 13:29
 **/
public interface Constant {
    String COMMON_TIME_PATTERN = "yyyy-MM-dd HH:mm:ss";
}
