package com.zj.gencode.beans;

import lombok.Builder;
import lombok.Data;

import java.util.List;

/**
 * 类
 *
 * @author xi.yang
 * @create 2019-12-19 14:56
 **/
@Data
@Builder
public class ClassField {
    private String packageName;
    private String name;
    /**
     * 注释
     */
    private String javaDoc;
    /**
     * 继承的类
     */
    private String extendClass;
    /**
     * 实现的类
     */
    private List<String> implClass;
    /**
     * 导入的类
     */
    private List<String> importClass;
    /**
     * 类上的注解
     */
    private List<String> annoClass;

    private List<PropField> propFields;
}
