package com.zj.gencode.beans;

import lombok.Builder;
import lombok.Data;

import java.util.List;

/**
 * 属性
 * @author xi.yang
 * @create 2019-12-19 14:52
 **/
@Data
@Builder
public class PropField {
    /**
     * 属性类型
     */
    private String fieldType;
    /**
     * 属性名
     */
    private String fieldName;
    /**
     * 注释
     */
    private String javaDoc;
    /**
     * 默认值
     */
    private String defValue;
    /**
     * 属性的注解
     */
    private List<String> annoClass;
}
