package com.zj.gencode.utils.bean;

import java.lang.reflect.Field;

/**
 * 属性拷贝
 *
 * @author xi.yang
 * @create 2018-12-14 11:04
 **/
public class BeanUtils {
    public static void copyProperties(Object from,Object to){
        Class fromClass = from.getClass();
        Class toClass = to.getClass();
        Field[] fromField = fromClass.getDeclaredFields();
        for (Field field : fromField) {
            field.setAccessible(true);
            if(field.isAnnotationPresent(CopyIgnore.class)){
                continue;
            }
            String fieldName = field.getName();
            Field toField = getField(toClass,fieldName);
            if(null == toField){
                continue;
            }
            try {
                toField.set(to,field.get(from));
            } catch (Exception e) {
            }
        }
    }

    private static Field getField(Class toClass,String fieldName){
        Field field = null;
        try {
            field = toClass.getDeclaredField(fieldName);
            if(null == field || field.isAnnotationPresent(CopyIgnore.class)){
                return null;
            }
        }catch (Exception e){
        }
        if(null != field){
            field.setAccessible(true);
        }
        return field;
    }
}
