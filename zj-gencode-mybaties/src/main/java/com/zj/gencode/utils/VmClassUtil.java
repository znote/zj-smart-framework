package com.zj.gencode.utils;

import com.zj.gencode.beans.ClassField;
import com.zj.gencode.utils.velocity.VmUtil;

import java.util.Collections;

/**
 * @author xi.yang
 * @create 2019-12-19 15:14
 **/
public class VmClassUtil {
    public static void genClass(String path, ClassField classField) {
        VmUtil.vmToFile("classVm.vm", Collections.singletonMap("data", classField), path);
    }
}
