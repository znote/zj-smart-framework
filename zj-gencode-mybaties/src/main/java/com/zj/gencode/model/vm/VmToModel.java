package com.zj.gencode.model.vm;

import com.zj.gencode.config.GenConfig;
import com.zj.gencode.model.excel.ExcelTable;
import com.zj.gencode.type.FieldType;
import com.zj.gencode.utils.StrUtils;
import com.zj.gencode.utils.bean.BeanUtils;
import lombok.Data;

import java.util.ArrayList;
import java.util.List;

/**
 * To数据模型
 *
 * @author xi.yang
 * @create 2018-12-14 10:25
 **/
@Data
public class VmToModel extends BaseModel {
    private List<PropField> propFields;
    private boolean haveDate;
    private String baseToPackageName;
    public VmToModel(ExcelTable excelTable) {
        BeanUtils.copyProperties(excelTable, this);
        this.setPackageName(GenConfig.instance.getToPackage());
        this.setBaseToPackageName(GenConfig.instance.getBaseToPackage());
        this.setClassName(StrUtils.underline2Camel(excelTable.getTableName(), false));
        this.setDescription(excelTable.getDescription());
        List<PropField> propFields = new ArrayList<>();
        for (ExcelTable.PropField propField : excelTable.getPropFields()) {
            if (propField.getFieldType() == null) {
                propField.setFieldType(propField.getSqlType().getFieldType());
            }

            PropField prop = new PropField(propField);
            if (FieldType.DATE.equals(propField.getFieldType())) {
                haveDate = true;
            }

            propFields.add(prop);
        }
        setPropFields(propFields);
    }

    @Data
    public class PropField{
        private String fieldType;
        private String fieldName;
        private String content;
        private String firstUpFieldName;
        /**
         * get方法标记，普通的就是get，bool类型是is
         */
        private String getMethodTarget = "get";

        private PropField(ExcelTable.PropField t) {
            this.fieldName = StrUtils.underline2Camel(t.getFieldName(), true);
            this.firstUpFieldName = StrUtils.underline2Camel(t.getFieldName(), false);
            this.content = t.getContent();
            if (FieldType.BOOLEAN.equals(t.getFieldType())) {
                this.getMethodTarget = "is";
            }
            if (FieldType.ID_ENUM.equals(t.getFieldType()) || FieldType.JAVA_BEAN.equals(t.getFieldType())) {
                this.fieldType = firstUpFieldName;
            } else {
                this.fieldType = StrUtils.underline2Camel(t.getFieldType().name(), false);
            }
        }
    }
}


