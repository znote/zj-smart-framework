package com.zj.gencode.model.excel;

import com.zj.gencode.type.FieldType;
import com.zj.gencode.type.SqlType;
import lombok.Data;

import java.util.List;

/**
 * 解析excel模型
 *
 * @author xi.yang
 * @create 2018-12-14 11:22
 **/
@Data
public class ExcelTable {
    /**
     * 表名
     */
    private String tableName;
    /**
     * 描述
     */
    private String description;
    private List<PropField> propFields;

    @Data
    public static class PropField{
        /**
         * sql属性名，“_”分隔
         */
        private String fieldName;
        /**
         * sql属性类型
         */
        private SqlType sqlType;
        /**
         * 属性长度限制
         */
        private int fieldLength;
        /**
         * 是否允许为空
         */
        private boolean allowNull;
        /**
         * 默认值
         */
        private String defaultValue;
        /**
         * 是否自动递增
         */
        private boolean autoIncrement;
        /**
         * 是否是主键
         */
        private boolean primary;
        /**
         * 是否是索引
         */
        private boolean index;
        /**
         * java类型
         */
        private FieldType fieldType;
        /**
         * 备注,注释
         */
        private String content;
    }
}


