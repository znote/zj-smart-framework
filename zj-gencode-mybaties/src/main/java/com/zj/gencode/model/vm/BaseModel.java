package com.zj.gencode.model.vm;

import com.zj.gencode.config.GenConfig;
import com.zj.gencode.type.Constant;
import lombok.Data;
import org.joda.time.DateTime;

/**
 * 基本模型
 *
 * @author xi.yang
 * @create 2018-12-14 10:06
 **/
@Data
public class BaseModel {
    public BaseModel() {
        this.author = GenConfig.instance.getAuthor();
        this.version = GenConfig.instance.getVersion();
    }

    /**
     * 包名
     */
    private String packageName;
    /**
     * 代码作者
     */
    private String author;
    /**
     * 版本号
     */
    private String version;
    /**
     * 描述
     */
    private String description;
    /**
     * 创建时间
     */
    private String createTime = DateTime.now().toString(Constant.COMMON_TIME_PATTERN);
    /**
     * 类名
     */
    private String className;
}
