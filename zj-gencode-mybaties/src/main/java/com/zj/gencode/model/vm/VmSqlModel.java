package com.zj.gencode.model.vm;

import com.zj.gencode.model.excel.ExcelTable;
import com.zj.gencode.utils.bean.BeanUtils;
import lombok.Data;

import java.util.List;
import java.util.stream.Collector;
import java.util.stream.Collectors;

/**
 * To数据模型
 *
 * @author xi.yang
 * @create 2018-12-14 10:25
 **/
@Data
public class VmSqlModel {
    private String tableName;
    /**
     * 描述
     */
    private String description;
    /**
     * 主键，包括是否自增
     */
    private String primaryKey;
    private List<PropField> propFields;

    public VmSqlModel(ExcelTable excelTable) {
        BeanUtils.copyProperties(excelTable, this);
        setPropFields(excelTable.getPropFields().stream().map(PropField::new).collect(Collectors.toList()));
        for (PropField propField : propFields) {
            if (propField.isPrimary()) {
                this.primaryKey = propField.getFieldName();
                break;
            }
        }
    }

    @Data
    public class PropField {
        private String fieldName;
        private String fieldType;
        /**
         * 是否自动递增
         */
        private boolean autoIncrement;
        private int fieldLength;
        private boolean allowNull;
        private String defaultValue;
        private String content;
        /**
         * 是否是主键
         */
        private boolean primary;

        private PropField(ExcelTable.PropField t) {
            BeanUtils.copyProperties(t, this);
            this.fieldType = t.getSqlType().getName();
            if (t.getFieldLength() > 0) {
                this.fieldLength = t.getFieldLength();
            } else if (t.getSqlType().getDefLength() != null) {
                this.fieldLength = t.getSqlType().getDefLength();
            }
        }
    }
}


