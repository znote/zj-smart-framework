package com.zj.gencode.config;

import com.zj.gencode.exception.GenException;
import lombok.Data;
import org.apache.commons.lang.StringUtils;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.util.Properties;

/**
 * 配置信息
 *
 * @author xi.yang
 * @create 2018-10-16 15:40
 **/
@Data
public class GenConfig {
    public static GenConfig instance;
    private static final String fileOutPathName = "file.out.path";
    private static final String fileExcelPathName = "file.excel.path";
    private static final String toPathName = "object.to.package";
    private static final String baseToPathName = "object.to.base.package";

    private static final String authorName = "config.base.author";
    private static final String versionName = "config.base.version";

    private static final String daoPathName = "object.dao.package";
    private static final String servicePathName = "object.service.package";
    private static final String serviceImplPathName = "object.service.impl.package";
    private String fileOutPath;
    private String fileExcelPath;
    private String toPackage;
    private String baseToPackage;

    private String author;
    private String version;

    private String daoPackage;
    private String servicePackage;
    private String serviceImplPackage;

    public static void init() {
        Properties properties = new Properties();
        try {
            properties.load(new FileInputStream(new File(getPropertiesFilePath())));
        } catch (IOException e) {
            throw new GenException("获取配置文件错误！请检查配置文件application.properties是否在resources目录下！");
        }
        GenConfig genConfig = new GenConfig();
        final String fileOutPath = properties.getProperty(fileOutPathName);
        if (StringUtils.isBlank(fileOutPath)) {
            throw new GenException("请配置文件输出位置");
        }
        genConfig.setFileOutPath(fileOutPath);
        final String excelPath = properties.getProperty(fileExcelPathName);
        if (StringUtils.isBlank(excelPath)) {
            throw new GenException("请配置excel文件位置");
        }
        genConfig.setFileExcelPath(excelPath);
        final String toPackage = properties.getProperty(toPathName);
        if (StringUtils.isBlank(toPackage)) {
            throw new GenException("请配置TO类的包名");
        }
        final String baseToPackage = properties.getProperty(baseToPathName);
        if (StringUtils.isBlank(baseToPackage)) {
            throw new GenException("请配置BaseTO类的包名");
        }
        genConfig.setBaseToPackage(baseToPackage);
        genConfig.setToPackage(toPackage);
        final String daoPackage = properties.getProperty(daoPathName);
        if (StringUtils.isBlank(daoPackage)) {
            genConfig.setDaoPackage(toPackage + ".dao");
        } else {
            genConfig.setDaoPackage(daoPackage);
        }
        final String servicePackage = properties.getProperty(servicePathName);
        if (StringUtils.isBlank(servicePackage)) {
            genConfig.setServicePackage(toPackage + ".service");
        } else {
            genConfig.setServicePackage(servicePackage);
        }
        final String serviceImplPackage = properties.getProperty(serviceImplPathName);
        if (StringUtils.isBlank(serviceImplPackage)) {
            genConfig.setServiceImplPackage(toPackage + ".service.impl");
        } else {
            genConfig.setServiceImplPackage(serviceImplPackage);
        }
        genConfig.setAuthor(properties.getProperty(authorName));
        genConfig.setVersion(properties.getProperty(versionName));
        genConfig.setVersion(properties.getProperty(versionName));
        instance = genConfig;
    }

    private static String getPropertiesFilePath() {
        return new File("").getAbsolutePath() + File.separator + "src" + File.separator + "main" + File.separator + "resources" + File.separator + "application.properties";
    }
}
