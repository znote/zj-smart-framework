# zj-gencode

#### 项目介绍
Java代码简易生成工具，根据excel生成sql、Java类、dao、mybaties模版等。
* 项目依赖[JDK8](http://www.oracle.com/technetwork/java/javase/downloads/index.html)
* excel解析依赖[poi](https://poi.apache.org/)
* 模版生成依赖[velocity](http://velocity.apache.org/)。
* 统一工具依赖[我的工具库](https://gitee.com/znote/zj-spring-boot-starter)，clone到本地后执行install.sh脚本即可。

#### 软件架构
本项目主要的流程为：
1. 解析excel提取数据
2. 数据验证
3. 转换成模版对象
4. 根据模版对象生成文件


#### 安装教程
目前直接pull代码运行Client即可，如有必要，后期考虑改为web项目。

#### 使用说明
1. 修改resource中config文件配置
2. 修改excel定义自己的模型
3. 运行Client即可
4. 拷贝生成文件到项目中，如果你也使用mybaties并使用了IdEnum，将dependency目录文件一起拷贝到对应位置

#### 参与贡献

1. Fork 本项目
2. 新建 Feat_xxx 分支
3. 提交代码
4. 新建 Pull Request

#### 意见和建议
直接提交issues或者入群交流
[QQ群![加入QQ群](http://oyx-mall-oss.oss-cn-chengdu.aliyuncs.com/mall/images/20200622/addQQGroup.png)](https://jq.qq.com/?_wv=1027&k=a06jnikh
)