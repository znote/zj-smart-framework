# zj-smart-framework

#### 介绍
微代码开发集成项目，

#### 食用方法
> 为了方便更新不对您已有功能造成影响，请按如下步骤食用本项目
1. clone 项目
2. 复制zj-admin改成自己的配置
3. 在复制zj-admin的自己项目引入自己需要的modules
4. 自己本地开发的模块建议另外定义一个modules文件夹
5. 随意更新项目到最新版本

#### 技术
* 核心框架：Springboot
* 模版语言：thymeleaf
* 数据库：mongodb、redis

#### 参与开发
1. fork项目
2. 提交变更到你自己的项目
3. 在本项目提issues
4. 对应issues做pr提交
> 具体操作可以参考博客：[如何参与开源项目](https://www.jianshu.com/p/c40812b0bda9)

#### 开发约定
1. 统一模块和方法名，不需要单独定义view的路由

#### 目录说明
- zj-util-base: 基础的工具类和模型定义库，不依赖任何库
- zj-util-javadoc: 解析代码注释的工具
- zj-admin: 基于这套集成项目开发的微代码后台，集成了oa和pm相关功能
- zj-gencode: 代码生成项目，用代码生成代码
- zj-spring-boot-starter: 基于springboot的各种组件封装集成

#### 开发计划
- [x] 统一路由规则实现
- [x] 菜单管理通过各子模块的简单配置自行维护
- [ ] 鉴权、角色、菜单的关系重构
- [ ] 根据view的model自动生成前端代码
- [ ] 根据后端的model自动生成服务端增删改查代码
- [ ] 内置模块：oa相关功能开发
- [ ] 内置模块：项目、任务、bug管理

> 功能正在开发中，希望大佬们积极参与进来，有什么好的想法也可以加在开发计划中，一起来完善！

#### 感谢
* [GVP若依/RuoYi](https://gitee.com/y_project/RuoYi)提供基本的脚手架和页面模版

#### 交流
欢迎各位技术大佬加入QQ群一起交流
[QQ![加入QQ群](http://oyx-mall-oss.oss-cn-chengdu.aliyuncs.com/mall/images/20200622/addQQGroup.png)](https://jq.qq.com/?_wv=1027&k=a06jnikh)