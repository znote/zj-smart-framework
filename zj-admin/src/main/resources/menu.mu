## 目录级别 目录名 路径标识 图标 增删改查(搜索)1248
### 项目管理
-	项目资源	shcms	fa fa-gear	15
--	项目管理	project	fa fa-gear	15
--	表单设计	table	fa fa-gear	15

### 系统管理
-	系统管理	system	fa fa-gear	15
--	用户管理	user	fa fa-user-o	15
--	角色管理	role	fa fa-user-secret	15
--	部门管理	dept	fa fa-outdent	15
--	岗位管理	post	fa fa-address-card-o	15
--	字典管理	dict	fa fa-bookmark-o	15
--	参数设置	config	fa fa-sun-o	15
--	通知公告	notice	fa fa-bullhorn	15

### 资源管理
-	资源管理	magnet	fa fa-gear	15
--	资源管理	movie	fa fa-bullhorn	15

### 系统工具
-	系统工具	tool	fa fa-bars	15
--	表单构建	build	fa fa-wpforms	15
--	代码生成	gen	fa fa-code	15
--	系统接口	swagger	fa fa-gg	15

### 系统监控
-	系统监控	monitor	fa fa-video-camera	15
--	在线用户	online	fa fa-user-circle	15
--	数据监控	data	fa fa-bug	15
--	服务监控	server	fa fa-server	15
--	缓存监控	cache	fa fa-cube	15
