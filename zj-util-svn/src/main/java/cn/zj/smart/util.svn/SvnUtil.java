package cn.zj.smart.util.svn;

import org.tmatesoft.svn.core.SVNException;
import org.tmatesoft.svn.core.wc.SVNClientManager;
import org.tmatesoft.svn.core.wc.SVNInfo;
import org.tmatesoft.svn.core.wc.SVNRevision;
import org.tmatesoft.svn.core.wc.SVNWCClient;

import java.io.File;

/**
 * @author xi.yang
 * @create 2021-10-18 14:49
 **/
public class SvnUtil {
    /**
     * 获取当前的svn版本号
     * @param path
     * @return
     */
    public static long getCurRevision(String path) {
        SVNWCClient svnwcClient = SVNClientManager.newInstance().getWCClient();
        try {
            SVNInfo svnInfo = svnwcClient.doInfo(new File(path), SVNRevision.WORKING);
            SVNRevision svnRevision = svnInfo.getCommittedRevision();
            return svnRevision.getNumber();
        } catch (SVNException e) {
            e.printStackTrace();
        }
        return 0;
    }
}
