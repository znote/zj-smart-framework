package cn.zj.smart.util.svn;

import org.junit.Test;

import java.io.File;
import java.util.Objects;

/**
 * @author xi.yang
 * @create 2021-10-21 15:29
 **/
public class SvnUtilTest {
    @Test
    public void testSvn() {
        String path = "D:\\C101\\SVNC101";
        File file = new File(path);
        if(!file.exists()){
            return;
        }
        for (File s : Objects.requireNonNull(file.listFiles())) {
            System.out.println(s.getAbsolutePath() + "====" + SvnUtil.getCurRevision(s.getAbsolutePath()));
        }
    }
}
