package cn.zj.smart.log4j2;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

/**
 * @author xi.yang
 * @create 2021-05-31 16:00
 **/
public class Log {
    public static final Logger log = LogManager.getLogger();
}
