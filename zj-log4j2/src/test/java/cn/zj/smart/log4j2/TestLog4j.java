package cn.zj.smart.log4j2;

/**
 * @author xi.yang
 * @create 2021-05-31 15:55
 **/
public class TestLog4j {
    public static void main(String[] args) {
        Log.log.info("==========info================");
        Log.log.warn("==========warn================");
        Log.log.error("==========error================");
        Log.log.trace("==========trace================");
    }
}
