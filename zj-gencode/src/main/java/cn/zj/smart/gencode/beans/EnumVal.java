package cn.zj.smart.gencode.beans;

import java.util.List;

/**
 * @author xi.yang
 * @create 2022-04-06 10:06
 **/
public class EnumVal {
    private String tag;
    private String desc;
    private List<String> objVal;

    public String getTag() {
        return tag;
    }

    public void setTag(String tag) {
        this.tag = tag;
    }

    public String getDesc() {
        return desc;
    }

    public void setDesc(String desc) {
        this.desc = desc;
    }

    public List<String> getObjVal() {
        return objVal;
    }

    public void setObjVal(List<String> objVal) {
        this.objVal = objVal;
    }
}
