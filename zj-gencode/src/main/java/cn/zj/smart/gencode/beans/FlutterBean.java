package cn.zj.smart.gencode.beans;

import java.lang.reflect.Type;
import java.util.Arrays;
import java.util.List;
import java.util.Set;

/**
 * 生成flutter的属性
 *
 * @author xi.yang
 * @create 2021-06-09 10:43
 **/
public class FlutterBean {
    private String name;
    private String desc;
    private Set<String> imports;
    private List<FlutterProp> flutterProp;

    public static enum FlutterType {
        fString("String", String.class),
        fInt("int", Integer.class, int.class, Long.class, long.class),
        fDouble("double", Double.class, double.class, Float.class, float.class),
        fBool("bool", Boolean.class, boolean.class),
        ;
        private String flutterName;
        private List<Class> clazz;

        FlutterType(String flutterName, Class... clazz) {
            this.flutterName = flutterName;
            this.clazz = Arrays.asList(clazz);
        }

        public static String findByClazz(Class clazz) {
            // 枚举在dart中用String就行
            if (clazz.isEnum()) {
                return fString.getFlutterName();
            }
            for (FlutterType value : FlutterType.values()) {
                if (value.getClazz().contains(clazz)) {
                    return value.getFlutterName();
                }
            }
            return clazz.getSimpleName();
        }

        public static boolean isBean(Class clazz) {
            if (clazz.isEnum()) {
                return false;
            }
            for (FlutterType value : FlutterType.values()) {
                if (value.getClazz().contains(clazz)) {
                    return false;
                }
            }
            return true;
        }

        public String getFlutterName() {
            return flutterName;
        }

        public List<Class> getClazz() {
            return clazz;
        }
    }

    public static class FlutterProp {
        private String typeName;
        private String name;
        private String desc;
        /**
         * 是否是对象类型
         */
        private boolean bean;
        /**
         * 是否是集合
         */
        private boolean array;

        public boolean isArray() {
            return array;
        }

        public void setArray(boolean array) {
            this.array = array;
        }

        public String getTypeName() {
            return typeName;
        }

        public void setTypeName(String typeName) {
            this.typeName = typeName;
        }

        public String getName() {
            return name;
        }

        public void setName(String name) {
            this.name = name;
        }

        public String getDesc() {
            return desc;
        }

        public boolean isBean() {
            return bean;
        }

        public void setBean(boolean bean) {
            this.bean = bean;
        }

        public void setDesc(String desc) {
            this.desc = desc;
        }
    }

    public static FlutterBeanBuilder builder() {
        return new FlutterBeanBuilder();
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDesc() {
        return desc;
    }

    public void setDesc(String desc) {
        this.desc = desc;
    }

    public List<FlutterProp> getFlutterProp() {
        return flutterProp;
    }

    public Set<String> getImports() {
        return imports;
    }

    public void setImports(Set<String> imports) {
        this.imports = imports;
    }

    public void setFlutterProp(List<FlutterProp> flutterProp) {
        this.flutterProp = flutterProp;
    }

    public static final class FlutterBeanBuilder {
        private String name;
        private String desc;
        private Set<String> imports;
        private List<FlutterProp> flutterProp;

        private FlutterBeanBuilder() {
        }

        public static FlutterBeanBuilder aFlutterBean() {
            return new FlutterBeanBuilder();
        }

        public FlutterBeanBuilder name(String name) {
            this.name = name;
            return this;
        }

        public FlutterBeanBuilder desc(String desc) {
            this.desc = desc;
            return this;
        }

        public FlutterBeanBuilder imports(Set<String> imports) {
            this.imports = imports;
            return this;
        }

        public FlutterBeanBuilder flutterProp(List<FlutterProp> flutterProp) {
            this.flutterProp = flutterProp;
            return this;
        }

        public FlutterBean build() {
            FlutterBean flutterBean = new FlutterBean();
            flutterBean.setName(name);
            flutterBean.setDesc(desc);
            flutterBean.setImports(imports);
            flutterBean.setFlutterProp(flutterProp);
            return flutterBean;
        }
    }
}
