package cn.zj.smart.gencode;

import cn.zj.smart.gencode.util.VmUtil;
import cn.zj.smart.util.FileUtil;
import cn.zj.smart.util.StringUtil;
import cn.zj.smart.util.eo.Regex;
import cn.zj.smart.util.net.HttpConnPool;
import cn.zj.smart.util.net.HttpUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.HashMap;
import java.util.Map;

/**
 * 初始化项目
 *
 * @author xi.yang
 * @create 2020-12-23 17:46
 **/
public class InitProject {
    private static final Logger log = LoggerFactory.getLogger(InitProject.class);

    public static void main(String[] args) {
        initModuleByName("cn.zj.smart.jacob","zj-jacob");
    }

    /**
     * 根据模块名初始化模块
     *
     * @param moduleName
     */
    public static void initModuleByName(String packageName, String moduleName) {
        String[] packages = StringUtil.splitByRegex(packageName, Regex.POINT);
        String[] params = new String[packages.length + 1];
        for (int i = 0; i < packages.length; i++) {
            params[i] = packages[i];
        }
        params[packages.length] = moduleName.replace("-", ".");
        final String packageNames = FileUtil.getPath(params);
        // 项目目录
//        final String basePath = FileUtil.getPath(FileUtil.getUserDir(), "zj-" + moduleName);
        final String basePath = FileUtil.getPath(FileUtil.getUserDir(), moduleName);
        FileUtil.mkdirs(basePath);
        // 项目文件夹
        final String basePkgPath = FileUtil.getPath(basePath, "src", "main", "java", packageNames);
//        FileUtil.mkdirs(FileUtil.getPath(basePkgPath, "beans"));
//        FileUtil.mkdirs(FileUtil.getPath(basePkgPath, "domain"));
//        FileUtil.mkdirs(FileUtil.getPath(basePkgPath, "repository"));
//        FileUtil.mkdirs(FileUtil.getPath(basePkgPath, "service"));
//        FileUtil.mkdirs(FileUtil.getPath(basePkgPath, "vo"));
//        FileUtil.mkdirs(FileUtil.getPath(basePkgPath, "util"));
        FileUtil.mkdirs(FileUtil.getPath(basePath, "src", "main", "java", packageNames));
        FileUtil.mkdirs(FileUtil.getPath(basePath, "src", "test", "java", packageNames));
        FileUtil.mkdirs(FileUtil.getPath(basePath, "src", "main", "resources"));
        FileUtil.mkdirs(FileUtil.getPath(basePath, "src", "main", "resources", "templates"));
        // pom文件
        Map<String, Object> dataMap = new HashMap<>();
        dataMap.put("moduleName", moduleName);
        VmUtil.vmToFile("vm/pom/pom.vm", dataMap, FileUtil.getPath(basePath, "pom.xml"));
        // README.md
        VmUtil.vmToFile("vm/readme/readme.vm", dataMap, FileUtil.getPath(basePath, "README.md"));
    }
}
