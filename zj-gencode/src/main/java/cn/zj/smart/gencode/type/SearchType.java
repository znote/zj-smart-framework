package cn.zj.smart.gencode.type;

/**
 * 搜索框显示类型
 * @author xi.yang
 * @create 2020-12-15 18:08
 **/
public enum SearchType {
    /**
     * 文本输入框
     */
    text
}
