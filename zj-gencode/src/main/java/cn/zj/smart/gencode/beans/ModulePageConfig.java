package cn.zj.smart.gencode.beans;

import cn.zj.smart.util.json.JsonUtil;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

/**
 * 页面数据配置
 *
 * @author xi.yang
 * @create 2020-12-15 17:59
 **/
public class ModulePageConfig {
    /**
     * 模块名称
     */
    private String moduleName;
    /**
     * 不要新增功能
     */
    private boolean ignoreAdd;
    /**
     * 不要删除功能
     */
    private boolean ignoreDel;
    /**
     * 不要编辑功能
     */
    private boolean ignoreEdit;
    private List<Integer> tests;
    private ModulePageConfig modulePageConfig;
    private List<ModulePageConfig> modulePageConfigs;

    public String getModuleName() {
        return moduleName;
    }

    public void setModuleName(String moduleName) {
        this.moduleName = moduleName;
    }

    public boolean isIgnoreAdd() {
        return ignoreAdd;
    }

    public void setIgnoreAdd(boolean ignoreAdd) {
        this.ignoreAdd = ignoreAdd;
    }

    public boolean isIgnoreDel() {
        return ignoreDel;
    }

    public void setIgnoreDel(boolean ignoreDel) {
        this.ignoreDel = ignoreDel;
    }

    public boolean isIgnoreEdit() {
        return ignoreEdit;
    }

    public void setIgnoreEdit(boolean ignoreEdit) {
        this.ignoreEdit = ignoreEdit;
    }

    public List<Integer> getTests() {
        return tests;
    }

    public void setTests(List<Integer> tests) {
        this.tests = tests;
    }

    public ModulePageConfig getModulePageConfig() {
        return modulePageConfig;
    }

    public void setModulePageConfig(ModulePageConfig modulePageConfig) {
        this.modulePageConfig = modulePageConfig;
    }

    public List<ModulePageConfig> getModulePageConfigs() {
        return modulePageConfigs;
    }

    public void setModulePageConfigs(List<ModulePageConfig> modulePageConfigs) {
        this.modulePageConfigs = modulePageConfigs;
    }

    public static void main(String[] args) {
        ModulePageConfig modulePageConfig = new ModulePageConfig();
        modulePageConfig.setIgnoreDel(true);
        modulePageConfig.setTests(Arrays.asList(1,2,3));
        ModulePageConfig other = new ModulePageConfig();
        other.setModuleName("12456");
        other.setIgnoreEdit(true);
        List<ModulePageConfig> modulePageConfigs = new ArrayList<>();
        for (int i = 0; i < 3; i++) {
            ModulePageConfig pear = new ModulePageConfig();
            modulePageConfig.setModuleName("test" + i);
            modulePageConfig.setIgnoreAdd(false);
            modulePageConfigs.add(pear);
        }
        modulePageConfig.setModulePageConfig(other);
        modulePageConfig.setModulePageConfigs(modulePageConfigs);
        System.out.println(JsonUtil.toString(modulePageConfig));
    }
}
