package cn.zj.smart.gencode.beans;

import java.util.List;
import java.lang.reflect.AnnotatedType;

/**
 * 类定义
 * @author xi.yang
 * @create 2019-12-19 14:56
 **/
public class ClassDef {
    /**
     * 包名
     */
    private String packageName;
    /**
     * 类名
     */
    private String name;
    /**
     * 注释
     */
    private String javaDoc;
    /**
     * 继承的类
     */
    private Class extendClass;
    /**
     * 实现的类
     */
    private List<Class> implClass;
    /**
     * 导入的类
     */
    private List<Class> importClass;
    /**
     * 类上的注解
     */
    private AnnotatedType[] annotatedTypes;
    /**
     * 属性
     */
    private List<PropDef> propFields;
    private List<EnumVal> enumVals;

    public List<EnumVal> getEnumVals() {
        return enumVals;
    }

    public void setEnumVals(List<EnumVal> enumVals) {
        this.enumVals = enumVals;
    }

    public static Builder builder() {
        return new Builder();
    }

    public String getPackageName() {
        return packageName;
    }

    public void setPackageName(String packageName) {
        this.packageName = packageName;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getJavaDoc() {
        return javaDoc;
    }

    public void setJavaDoc(String javaDoc) {
        this.javaDoc = javaDoc;
    }

    public Class getExtendClass() {
        return extendClass;
    }

    public void setExtendClass(Class extendClass) {
        this.extendClass = extendClass;
    }

    public List<Class> getImplClass() {
        return implClass;
    }

    public void setImplClass(List<Class> implClass) {
        this.implClass = implClass;
    }

    public List<Class> getImportClass() {
        return importClass;
    }

    public void setImportClass(List<Class> importClass) {
        this.importClass = importClass;
    }

    public AnnotatedType[] getAnnotatedTypes() {
        return annotatedTypes;
    }

    public void setAnnotatedTypes(AnnotatedType[] annotatedTypes) {
        this.annotatedTypes = annotatedTypes;
    }

    public List<PropDef> getPropFields() {
        return propFields;
    }

    public void setPropFields(List<PropDef> propFields) {
        this.propFields = propFields;
    }

    public static final class Builder {
        private String packageName;
        private String name;
        private String javaDoc;
        private Class extendClass;
        private List<Class> implClass;
        private List<Class> importClass;
        private AnnotatedType[] annotatedTypes;
        private List<PropDef> propFields;

        private Builder() {
        }

        public static Builder aClassDef() {
            return new Builder();
        }

        public Builder packageName(String packageName) {
            this.packageName = packageName;
            return this;
        }

        public Builder name(String name) {
            this.name = name;
            return this;
        }

        public Builder javaDoc(String javaDoc) {
            this.javaDoc = javaDoc;
            return this;
        }

        public Builder extendClass(Class extendClass) {
            this.extendClass = extendClass;
            return this;
        }

        public Builder implClass(List<Class> implClass) {
            this.implClass = implClass;
            return this;
        }

        public Builder importClass(List<Class> importClass) {
            this.importClass = importClass;
            return this;
        }

        public Builder annotatedTypes(AnnotatedType[] annotatedTypes) {
            this.annotatedTypes = annotatedTypes;
            return this;
        }

        public Builder propFields(List<PropDef> propFields) {
            this.propFields = propFields;
            return this;
        }

        public ClassDef build() {
            ClassDef classDef = new ClassDef();
            classDef.setPackageName(packageName);
            classDef.setName(name);
            classDef.setJavaDoc(javaDoc);
            classDef.setExtendClass(extendClass);
            classDef.setImplClass(implClass);
            classDef.setImportClass(importClass);
            classDef.setAnnotatedTypes(annotatedTypes);
            classDef.setPropFields(propFields);
            return classDef;
        }
    }
}
