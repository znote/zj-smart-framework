package cn.zj.smart.gencode.beans;

import java.lang.reflect.AnnotatedType;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * 属性定义
 * @author xi.yang
 * @create 2019-12-19 14:52
 **/
public class PropDef {
    /**
     * 属性类型
     */
    private Class fieldType;
    /**
     * 属性名
     */
    private String fieldName;
    /**
     * 注释
     */
    private String javaDoc;
    /**
     * 默认值
     */
    private String defValue;
    /**
     * 属性的注解
     */
    private AnnotatedType[] annotatedTypes;
    /**
     * 是否是数组
     */
    private boolean array;

    public String getGetMethodTarget(){
        if (fieldType.equals(boolean.class) || fieldType.equals(Boolean.class)) {
            return "is";
        }
        return "get";
    }

    public String getFirstUpFieldName() {
        return underline2Camel(this.fieldName, false);
    }

    /**
     * 下划线转驼峰法
     * @param line 源字符串
     * @param smallCamel 大小驼峰,是否为小驼峰
     * @return 转换后的字符串
     */
    private static String underline2Camel(String line,boolean smallCamel){
        if(line==null||"".equals(line)){
            return "";
        }
        StringBuffer sb=new StringBuffer();
        Pattern pattern=Pattern.compile("([A-Za-z\\d]+)(_)?");
        Matcher matcher=pattern.matcher(line);
        while(matcher.find()){
            String word=matcher.group();
            sb.append(smallCamel&&matcher.start()==0?Character.toLowerCase(word.charAt(0)):Character.toUpperCase(word.charAt(0)));
            int index=word.lastIndexOf('_');
            if(index>0){
                sb.append(word.substring(1, index).toLowerCase());
            }else{
                sb.append(word.substring(1).toLowerCase());
            }
        }
        return sb.toString();
    }

    public boolean isArray() {
        return array;
    }

    public void setArray(boolean array) {
        this.array = array;
    }

    public String getFieldName() {
        return fieldName;
    }
    public void setFieldName(String fieldName) {
        this.fieldName = fieldName;
    }
    public String getJavaDoc() {
        return javaDoc;
    }

    public void setJavaDoc(String javaDoc) {
        this.javaDoc = javaDoc;
    }

    public String getDefValue() {
        return defValue;
    }

    public void setDefValue(String defValue) {
        this.defValue = defValue;
    }

    public AnnotatedType[] getAnnotatedTypes() {
        return annotatedTypes;
    }

    public Class getFieldType() {
        return fieldType;
    }

    public void setFieldType(Class fieldType) {
        this.fieldType = fieldType;
    }

    public void setAnnotatedTypes(AnnotatedType[] annotatedTypes) {
        this.annotatedTypes = annotatedTypes;
    }

    public static PropDefBuilder builder() {
        return new PropDefBuilder();
    }

    public static final class PropDefBuilder {
        private Class fieldType;
        private String fieldName;
        private String javaDoc;
        private String defValue;
        private AnnotatedType[] annotatedTypes;
        private boolean array;

        private PropDefBuilder() {
        }

        public PropDefBuilder fieldType(Class fieldType) {
            this.fieldType = fieldType;
            return this;
        }

        public PropDefBuilder fieldName(String fieldName) {
            this.fieldName = fieldName;
            return this;
        }

        public PropDefBuilder javaDoc(String javaDoc) {
            this.javaDoc = javaDoc;
            return this;
        }

        public PropDefBuilder defValue(String defValue) {
            this.defValue = defValue;
            return this;
        }

        public PropDefBuilder annotatedTypes(AnnotatedType[] annotatedTypes) {
            this.annotatedTypes = annotatedTypes;
            return this;
        }

        public PropDefBuilder array(boolean array) {
            this.array = array;
            return this;
        }

        public PropDef build() {
            PropDef propDef = new PropDef();
            propDef.setFieldType(fieldType);
            propDef.setFieldName(fieldName);
            propDef.setJavaDoc(javaDoc);
            propDef.setDefValue(defValue);
            propDef.setAnnotatedTypes(annotatedTypes);
            propDef.setArray(array);
            return propDef;
        }
    }
}
