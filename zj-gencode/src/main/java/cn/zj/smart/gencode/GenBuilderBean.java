package cn.zj.smart.gencode;

import cn.zj.smart.gencode.beans.ClassDef;
import cn.zj.smart.gencode.beans.ModulePageConfig;
import cn.zj.smart.gencode.beans.PropDef;
import cn.zj.smart.gencode.util.VmUtil;
import cn.zj.smart.util.javadoc.beans.JavaDoc;
import cn.zj.smart.util.javadoc.util.JavaDocUtil;

import java.lang.reflect.Field;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * 根据精简Java类生成带get/set/builder的完整类
 */
public class GenBuilderBean {
    public static void main(String[] args) {
        ClassDef classDef = getJavaClassDef(ModulePageConfig.class);

        System.out.println(classDef);

        String vmName = "./vm/java/javaWithBuilder.vm";
        Map<String, Object> map = new HashMap<>();
        map.put("data", classDef);
        String outPath = "E:\\zj-smart-framework\\zj-gencode\\src\\main\\resources\\vm\\java\\ModulePageConfig.java";
        VmUtil.vmToFile(vmName,map,outPath);
    }

    private static ClassDef getJavaClassDef(Class clazz) {
        JavaDoc javaDoc = JavaDocUtil.getClassJavaDoc(clazz);
        List<PropDef> propDefs = new ArrayList<>();
        for (Field field : clazz.getDeclaredFields()) {
            propDefs.add(PropDef.builder()
                    .fieldName(field.getName())
                    .fieldType(JavaDocUtil.getActualType(field))
                    .javaDoc(javaDoc.getPropertyDocs().get(field.getName()))
                    .build());
        }
        return ClassDef.builder()
                .name(clazz.getSimpleName())
                .extendClass(clazz.getSuperclass())
                .packageName(clazz.getPackage().getName())
                .javaDoc(javaDoc.getClassDoc())
                .propFields(propDefs)
                .build();
    }
}
