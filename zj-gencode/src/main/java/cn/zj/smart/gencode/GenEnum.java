package cn.zj.smart.gencode;

import cn.zj.smart.gencode.beans.ClassDef;
import cn.zj.smart.gencode.beans.EnumVal;
import cn.zj.smart.gencode.beans.PropDef;
import cn.zj.smart.gencode.util.VmUtil;
import cn.zj.smart.util.excel.ExcelUtil;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * 表格最后一列不能为空
 * @author xi.yang
 * @create 2022-04-02 12:30
 **/
public class GenEnum {
    public static final String packageName = "xag.idgo.core.storage.bean.game.maze.type";
    public static void main(String[] args) {
        String vmName = "vm/java/enum.vm";
        Map<String, List<List<String>>> enums = ExcelUtil.getAllExcel2MapList("D:\\C1\\game1_share_ml\\game\\MasterData\\Excel\\base_config\\enums.xlsx");
        System.out.println(enums);
        enums.forEach((k,v)->{
            ClassDef classDef = new ClassDef();
            classDef.setName(k);
            // 注入类注释
            String classDesc = v.get(0).get(0);
            classDef.setJavaDoc(classDesc);
            // 字段
            List<String> filedNames = v.get(3);
            // 注释
            List<String> desc = v.get(1);
            // 类型
            List<String> types = v.get(2);
            List<PropDef> propFields = new ArrayList<>();
            for (int i = 0; i < filedNames.size(); i++) {
                // 枚举值不做属性,属性描述也略过
                if (i == 1 || i == 2) {
                    continue;
                }
                PropDef propDef = new PropDef();
                propDef.setFieldName(filedNames.get(i));
                propDef.setJavaDoc(desc.get(i));
                propDef.setFieldType(getType(types.get(i)));
                propFields.add(propDef);
            }
            classDef.setPackageName(packageName);
            classDef.setPropFields(propFields);
            List<EnumVal> enumVals = new ArrayList<>();
            for (int i = 4; i < v.size(); i++) {
                List<String> vals = v.get(i);
                EnumVal enumVal = new EnumVal();
                enumVal.setTag(vals.get(1));
                enumVal.setDesc(vals.get(2));
                List<String> vs = new ArrayList<>();
                int t = 0;
                for (int j = 0; j < vals.size(); j++) {
                    // 枚举值不做属性,属性描述也略过
                    if (j == 1 || j == 2) {
                        continue;
                    }
                    PropDef propDef = propFields.get(t);
                    if (propDef.getFieldType().equals(int.class) || propDef.getFieldType().equals(Integer.class) || propDef.getFieldType().equals(long.class) || propDef.getFieldType().equals(Long.class)) {
                        vs.add(new BigDecimal(vals.get(j)).longValue() + "");
                    }else if(propDef.getFieldType().equals(boolean.class) || propDef.getFieldType().equals(Boolean.class)){
                        if (new BigDecimal(vals.get(j)).intValue() == 0) {
                            vs.add("false");
                        }else {
                            vs.add("true");
                        }
                    } else {
                        vs.add(vals.get(j));
                    }
                    t++;
                }
                enumVal.setObjVal(vs);
                enumVals.add(enumVal);
            }
            classDef.setEnumVals(enumVals);
            Map<String, Object> map = new HashMap<>();
            map.put("data", classDef);
            String outPath = "D:\\C1\\xa-game-idle\\server\\src\\main\\java\\xag\\idgo\\core\\storage\\bean\\game\\maze\\type\\"+k+".java";
            VmUtil.vmToFile(vmName,map,outPath);
        });
    }

    private static Class getType(String s) {
        if ("i".equals(s) || "int".equals(s)) {
            return int.class;
        }
        if ("l".equals(s) || "long".equals(s)) {
            return int.class;
        }
        if ("boolean".equals(s) || "b".equals(s)) {
            return boolean.class;
        }
        if (s.contains("enum")) {
            return Enum.class;
        }
        return String.class;
    }
}
