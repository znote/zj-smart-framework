package cn.zj.smart.gencode.beans;

/**
 * 前端模版类属性上的注解
 * @author xi.yang
 * @create 2020-12-15 17:58
 **/
public @interface TableProp {
    /**
     * 是否作为搜索条件
     * @return
     */
    boolean search() default false;
    /**
     * 是否显示在列表
     * @return
     */
    boolean view() default true;
}
