package cn.zj.smart.jacob;

import com.jacob.activeX.ActiveXComponent;
import com.jacob.com.Dispatch;
import com.jacob.com.Variant;

public class JacobUtil {
    //调用windowsApi 的 com组件，Sapi.spVoice是 windows com组件名称
    private static final JacobUtil instance = new JacobUtil();
    private ActiveXComponent activeXComponent;
    //从com组件中获得调度目标
    private Dispatch dis;
    private Variant contentVariant;
    private JacobUtil() {
    }

    public static JacobUtil getInstance(){
        return instance;
    }

    public void speak(String content){
        contentVariant = new Variant(content);
        Dispatch.call(dis, "Speak", contentVariant);
    }

    public void init(){
        activeXComponent = new ActiveXComponent("Sapi.SpVoice");
        dis = activeXComponent.getObject();
        //设置语言组件属性
        activeXComponent.setProperty("Volume", new Variant(100));
        activeXComponent.setProperty("Rate", new Variant(-1));
    }

    public void destroy(){
        dis.safeRelease();
        activeXComponent.safeRelease();
    }
}
