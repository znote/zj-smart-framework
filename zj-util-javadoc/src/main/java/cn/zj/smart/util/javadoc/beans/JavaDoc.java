package cn.zj.smart.util.javadoc.beans;

import java.io.Serializable;
import java.util.Map;

/**
 * 解析类的注释
 * @author xi.yang
 * @create 2019-08-16 14:58
 **/
public class JavaDoc implements Serializable {
    /**
     * 包名
     */
    private String packageName;
    /**
     * 类名
     */
    private String className;
    /**
     * 类的注释
     */
    private String classDoc;
    /**
     * 属性的注释
     */
    private Map<String, String> propertyDocs;
    /**
     * 方法的注释
     */
    private Map<String, String> methodDocs;
    /**
     * 获取完整的类名
     * @return
     */
    public String getPackageClassName() {
        return packageName + "." + className;
    }

    public String getPackageName() {
        return packageName;
    }

    public void setPackageName(String packageName) {
        this.packageName = packageName;
    }

    public String getClassName() {
        return className;
    }

    public void setClassName(String className) {
        this.className = className;
    }

    public String getClassDoc() {
        return classDoc;
    }

    public void setClassDoc(String classDoc) {
        this.classDoc = classDoc;
    }

    public Map<String, String> getPropertyDocs() {
        return propertyDocs;
    }

    public void setPropertyDocs(Map<String, String> propertyDocs) {
        this.propertyDocs = propertyDocs;
    }

    public Map<String, String> getMethodDocs() {
        return methodDocs;
    }

    public void setMethodDocs(Map<String, String> methodDocs) {
        this.methodDocs = methodDocs;
    }

    public static Builder builder() {
        return new Builder();
    }

    public static final class Builder {
        private String packageName;
        private String className;
        private String classDoc;
        private Map<String, String> propertyDocs;

        private Map<String, String> methodDocs;

        private Builder() {
        }

        public Builder packageName(String packageName) {
            this.packageName = packageName;
            return this;
        }

        public Builder className(String className) {
            this.className = className;
            return this;
        }

        public Builder classDoc(String classDoc) {
            this.classDoc = classDoc;
            return this;
        }

        public Builder propertyDocs(Map<String, String> propertyDocs) {
            this.propertyDocs = propertyDocs;
            return this;
        }

        public Builder methodDocs(Map<String, String> methodDocs) {
            this.methodDocs = methodDocs;
            return this;
        }

        public JavaDoc build() {
            JavaDoc javaDoc = new JavaDoc();
            javaDoc.setPackageName(packageName);
            javaDoc.setClassName(className);
            javaDoc.setClassDoc(classDoc);
            javaDoc.setPropertyDocs(propertyDocs);
            javaDoc.setMethodDocs(methodDocs);
            return javaDoc;
        }
    }
}
