package cn.zj.smart.util.javadoc;

import cn.zj.smart.log4j2.Log;
import cn.zj.smart.util.javadoc.beans.JavaDoc;
import cn.zj.smart.util.javadoc.util.JavaDocUtil;
import org.junit.Test;

/**
 * @author xi.yang
 * @create 2021-02-20 14:39
 **/
public class JavaDocUtilTest {
    @Test
    public void getClassJavaDocTest() {
        JavaDoc javaDoc = JavaDocUtil.getClassJavaDoc(JavaDoc.class);
        Log.log.info(javaDoc);
    }
}
