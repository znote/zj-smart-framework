package cn.zj.smart.util.json;

import com.alibaba.fastjson.JSON;

import java.util.List;

/**
 * json相关工具
 * @author xi.yang
 * @create 2021-05-12 14:07
 **/
public class JsonUtil {
    private static final String DATE_FORMATTER = "yyyy-MM-dd HH:mm:ss";
    public static String toString(Object o) {
        return JSON.toJSONStringWithDateFormat(o, DATE_FORMATTER);
    }

    public static  <T> T parse(String jsonStr, Class<T> clazz) {
        return JSON.parseObject(jsonStr, clazz);
    }

    public static  <T> List<T> parseList(String jsonStr, Class<T> clazz) {
        return JSON.parseArray(jsonStr, clazz);
    }
}
